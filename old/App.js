import React from 'react';
// import AppLoading from 'expo-app-loading';
import { Container, Text } from 'native-base';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import GoOutDog from './src';
import { WalkContext } from './src/context';
import { Provider } from 'react-redux';
// import configStore, { sagaMiddleware } from './store';
import configStore  from './store';
// import initSagas from './initSagas'; 

export const store = configStore();
// initSagas(sagaMiddleware);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      walks: [
        {
          start: '',
          end: '',
          poo: 1,
        }
      ],
      setWalks: this.setWalks
    };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    this.setState({ isReady: true });
  }

  setWalks = walks => {
    this.setState({ walks });
  };


  render() {
    if (!this.state.isReady) {
      return <Text>loading</Text>;
    }
    return (
      <Provider store={store} >
          <GoOutDog />
      </Provider>
    );
  }
}

export default App;