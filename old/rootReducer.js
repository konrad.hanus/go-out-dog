import { combineReducers } from 'redux';
import testReducer from './src/containers/Test/reducer';
import userReducer from './src/containers/RegistrationForm/reducer';
import loginReducer from './src/containers/LogIn/reducer';
import walksReducer from './src/containers/Walk/reducer';
import currentWalkId from './src/containers/CurrentWalkId/reducer';
import usersReducer from './src/containers/Users/reducer';
import keepersReducer from './src/containers/Keepers/reducer';
import ownersReducer from './src/containers/Owners/reducer';
import swapReducer from './src/containers/Swap/reducer';

export default combineReducers({
    test: testReducer, 
    walks: walksReducer,
    currentWalkId: currentWalkId,
    user: userReducer,
    login: loginReducer,
    users: usersReducer,
    keepers: keepersReducer,
    owners: ownersReducer,
    swap: swapReducer,
})
