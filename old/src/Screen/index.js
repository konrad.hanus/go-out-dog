import React, { Component } from 'react';
import Footer from './../Footer';
import { Container, Content, Text } from "native-base";

export default class Screen extends Component {


    
    render() {
        return (
            <Container>
                <Content>
                    {React.Children.map(this.props.children, child => {
                        if (React.isValidElement(child)) {
                            return React.cloneElement(child, this.props);
                        }
                        return child;
                    })}
                </Content>
                {this.props.current !== "LogIn" && this.props.current !== "RegistrationForm" && this.props.current !== "LogOut" && <Footer navigation={this.props.navigation} current={this.props.current} />}
            </Container>
        );
    }
}


