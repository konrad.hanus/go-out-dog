import React, { Component } from 'react';
import { Content, List, ListItem, Text, Body, Right } from 'native-base';
import { connect } from 'react-redux';
import {YOU} from './../containers/Walk';

class Points extends Component {


    you = () => <List>
        <ListItem thumbnail>
            {/* <Left>
        <Thumbnail square source={{ uri: 'Image URL' }} />
    </Left> */}
            <Body>
                <Text>{this.props.coowner}</Text>
            </Body>
            <Right>
                <Text>{this.props.coownersWalks}pkt</Text>
            </Right>
        </ListItem>
        <ListItem thumbnail>
            {/* <Left>
    <Thumbnail square source={{ uri: 'Image URL' }} />
</Left> */}
            <Body>
                <Text>{YOU}</Text>
            </Body>
            <Right>
                <Text>{this.props.yourWalks}pkt</Text>
            </Right>
        </ListItem>
    </List>
        ;

    coowner = () => <List><ListItem thumbnail>
        {/* <Left>
    <Thumbnail square source={{ uri: 'Image URL' }} />
</Left> */}
        <Body>
            <Text>{YOU}</Text>
        </Body>
        <Right>
            <Text>{this.props.yourWalks}pkt</Text>
        </Right>
    </ListItem>
        <ListItem thumbnail>
            {/* <Left>
        <Thumbnail square source={{ uri: 'Image URL' }} />
    </Left> */}
            <Body>
                <Text>{this.props.coowner}</Text>
            </Body>
            <Right>
                <Text>{this.props.coownersWalks}pkt</Text>
            </Right>
        </ListItem>
    </List>;
    render() {
        return (
            <Content>
                <Content>
                    {this.props.coownersWalks < this.props.yourWalks ? this.coowner() : this.you()}
                </Content>

            </Content>
        );
    }
}

export default connect(
    ((state) => {


        const coownerId = state.owners && state.owners[0] ? state.owners[0].id : null;
        const yourId = state.user && state.user.user.uid;
        function isWalkByKrysia(walk) {
            return walk.owner === coownerId;
        }

        function isWalkByKonrad(walk) {
            return walk.owner === yourId;
        }

        const yoursWalks = state.walks.filter(isWalkByKonrad);

        const coownersWalks = state.walks.filter(isWalkByKrysia);

        let yourPoints = 0;
        yoursWalks.forEach((i) => {
            yourPoints += (i.poo * 100) + (i.duration ? i.duration : 0)
        })
        let coownerPoints = 0;
        coownersWalks.forEach((i) => {
            coownerPoints += (i.poo * 100) + (i.duration ? i.duration : 0)
        })


        return ({
            yourWalks: yourPoints,
            coownersWalks: coownerPoints,
            coowner: state.owners && state.owners[0] ? state.owners[0].email.split('@')[0] : null,
        })
    })
)(Points);


