export const userId = (state) => state.user.user.uid
export const userEmail = (state) => state.user.user.email
export const coOwnerId = (state) => state.owners[0] ? state.owners[0].id : null 
export const orderOwner = (state) => state.owners[0] ? state.owners[0].order : null 
