import 'react-native-gesture-handler';
import React, { useState } from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Screen from './Screen';

import Walk from './containers/Walk';
import Walking from './containers/Walking';
import Points from './Points';
import Keepers from './containers/Keepers';
import Friends from './containers/Friends';
import Swap from './containers/Swap';
import LogIn from './containers/LogIn';
import LogOut from './containers/LogOut';
import RegistrationForm from './containers/RegistrationForm';
import Search from './containers/Search';
import DogSummary from './containers/DogSummary';
const Stack = createStackNavigator();

function App() {

  const [current, setCurrent] = useState(0);
  const [currentWalk, setCurrentWalk] = useState(0);

  return (
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen name="LogIn"  options={{
          title: 'Start',
          headerStyle: {
            backgroundColor: '#32CD32',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}>
            {props => <Screen {...props} setCurrentWalk={setCurrentWalk} current={"LogIn"}><LogIn /></Screen>}
        </Stack.Screen>
        <Stack.Screen name="LogOut"  options={{
          title: 'Wylogowanie',
          headerStyle: {
            backgroundColor: '#32CD32',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}>
            {props => <Screen {...props} setCurrentWalk={setCurrentWalk} current={"LogOut"}><LogOut /></Screen>}
        </Stack.Screen>
        <Stack.Screen name="RegistrationForm"  options={{
          title: 'Rejestracja',
          headerStyle: {
            backgroundColor: '#32CD32',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}>
            {props => <Screen {...props} setCurrentWalk={setCurrentWalk} current={"RegistrationForm"}><RegistrationForm /></Screen>}
        </Stack.Screen>
      <Stack.Screen name="Walk"  options={{
          title: 'Spacery',
          headerStyle: {
            backgroundColor: '#32CD32',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}>
            {props => <Screen {...props} setCurrentWalk={setCurrentWalk} current={"Walk"}><Walk/></Screen>}
        </Stack.Screen>
        <Stack.Screen name="Points" 
        options={{
          title: 'Punkty',
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}  >
            {props => <Screen {...props} current={"Points"}><Points/></Screen>}
        </Stack.Screen>

        <Stack.Screen name="Walking" options={{
          title: 'Na spacerze',
          headerStyle: {
            backgroundColor: '#565656',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} >
            {props => <Screen {...props} currentWalk={currentWalk} current={"Walk"} ><Walking/></Screen>}
        </Stack.Screen>
        
        
        <Stack.Screen name="Swap"  options={{
          title: 'Zamiany',
          headerStyle: {
            backgroundColor: '#b803ff',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}>
            {props => <Screen {...props} current={"Swap"} ><Swap/></Screen>}
        </Stack.Screen>

       
        <Stack.Screen name="Keepers" options={{
          title: 'Opiekunowie',
          headerStyle: {
            backgroundColor: '#565656',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} >
            {props => <Screen {...props} current={"Keepers"} ><Keepers/></Screen>}
        </Stack.Screen>

        <Stack.Screen name="Friends" options={{
          title: 'Znajomi',
          headerStyle: {
            backgroundColor: '#565656',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} >
            {props => <Screen {...props} current={"Friends"} ><Friends/></Screen>}
        </Stack.Screen>

        <Stack.Screen name="Search" options={{
          title: 'Szukaj znajomych',
          headerStyle: {
            backgroundColor: '#565656',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} >
            {props => <Screen {...props} current={"Search"} ><Search/></Screen>}
        </Stack.Screen>

        <Stack.Screen name="DogSummary" options={{
          title: 'Pies',
          headerStyle: {
            backgroundColor: '#565656',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} >
            {props => <Screen {...props} current={"DogSummary"} ><DogSummary/></Screen>}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;