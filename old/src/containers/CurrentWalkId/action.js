export const SET_CURRENT_WALK_ID = '@@CURRENT_WALK/SET_ID';

export const actionCreators = {
    setCurrentWalkId: ({ id }) => {
        return ({
            type: SET_CURRENT_WALK_ID,
            payload:
            {
                id,
            }
        })
    }
};