import { SET_CURRENT_WALK_ID } from './action';

function currentWalkId(state = null, action) {
    switch (action.type) {
        
        case SET_CURRENT_WALK_ID:
          return action.payload.id;
    }
    return state;
}

export default currentWalkId;