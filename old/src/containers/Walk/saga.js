import { takeEvery, call, all, fork, put, select } from 'redux-saga/effects';
import { SAVE_WALKS, SAVE_WALK, FETCH_WALKS_DB, REMOVE_WALK } from './action';
import firebase from 'firebase';
import ReduxSagaFirebase from 'redux-saga-firebase';
import apiKeys from '../../constants/apiKeys';
import { actionCreators } from './action';
import { userId, coOwnerId, orderOwner } from '../../selectors/user';

const myFirebaseApp = !firebase.apps.length ? firebase.initializeApp(apiKeys.FirebaseConfig) : firebase.app();
export const reduxSagaFirebase = new ReduxSagaFirebase(myFirebaseApp);

function* onSaveWalksSaga(action) {
    const getUserId = yield select(userId);
    yield call(reduxSagaFirebase.database.update, getUserId + '/walks', action.payload);

}

function* onSaveWalkSaga(action) {

   
    let getUserId;  
    if(!action.meta)
    {
        getUserId = yield select(userId);
    }else{
      
        getUserId = action.meta;
 
    } 
    yield call(reduxSagaFirebase.database.update, getUserId + '/walks/' + action.payload.id, action.payload);

}

function* onRemoveWalkSaga(action) {

    let getUserId;  
    if(!action.meta)
    {
        getUserId = yield select(userId);
    }else{
       
        getUserId = action.meta;
     
    } 
    yield call(reduxSagaFirebase.database.delete, getUserId + '/walks/' + action.payload.id);

}



function* onFetchWalksSaga() {

    const getUserId = yield select(userId);
    let walks = yield call(reduxSagaFirebase.database.read, getUserId + '/walks');
    if (!walks) {
        walks = []
    } else {
        walks = Object.keys(walks).map(id => walks[id]);
    }

   
    const getCoOwnerId = yield select(coOwnerId);
    const getOrderOwner = yield select(orderOwner);

    let ownersWalks = [];

    if (getCoOwnerId) {
        ownersWalks = yield call(reduxSagaFirebase.database.read, getCoOwnerId + '/walks');

    }
    if (!ownersWalks) {
        ownersWalks = [];
    } else {
        ownersWalks = Object.keys(ownersWalks).map(id => ownersWalks[id]);
       
    }

    let concatWalks = walks.concat(ownersWalks);

    if(getOrderOwner === 2)
    {
        concatWalks = ownersWalks.concat(walks);
    }

    yield put(actionCreators.putWalks(concatWalks));

}


function* saveWalksSaga() {
    yield takeEvery(SAVE_WALKS, onSaveWalksSaga);
}

function* saveWalkSaga() {
    yield takeEvery(SAVE_WALK, onSaveWalkSaga);
}

function* fetchWalksSaga() {
    yield takeEvery(FETCH_WALKS_DB, onFetchWalksSaga);
}

function* removeWalkSaga() {
    yield takeEvery(REMOVE_WALK, onRemoveWalkSaga);
}

export function* walkSaga() {
    yield all([fork(saveWalksSaga), fork(fetchWalksSaga), fork(saveWalkSaga), fork(removeWalkSaga)]);
}
