import React, { Component } from 'react';
import { Content, Card, CardItem, Text, Icon, Button, Left, Right, Body } from 'native-base';
import moment from 'moment';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import { actionCreators } from './action';
import WalkItem from './../../components/WalkItem';
import WalkHeader from './../../components/WalkHeader';
import { actionCreators as actionCreatorsSetCurrentWalkId } from './../CurrentWalkId/action';
import { AsyncStorage } from 'react-native';
import Test from '../Test';
import userReducer from '../RegistrationForm/reducer';
import { actionCreators as keppersActionCreators } from '../Keepers/action';
import { actionCreators as ownersActionCreators } from '../Owners/action';
import { actionCreators as swapActionCreators } from '../Swap/action';
import SwapHeader from '../SwapHeader';
import FindMe from '../FindMe';
import app from '../../../app.json';

const MORNING = 'morinig';
const AFTERNOON = 'afternoon';
const EVENING = 'evening';
export const YOU = "Ty";
moment.locale('pl');

class Walk extends Component {

    constructor(props) {
        super(props)

        this.state = {
            currentDay: moment(),
            today: moment(),
            [MORNING]: null,
            [AFTERNOON]: null,
            [EVENING]: null,
            walks: this.props.walks
        }
    }


    goBack = () => {
        const dayBack = this.state.currentDay.add(-1, 'days');
        this.setState({ currentDay: dayBack })
    }

    goNext = () => {
        const dayNext = this.state.currentDay.add(1, 'days');
        this.setState({ currentDay: dayNext })

    }

    _retrieveData = async (key) => {
        try {

            const walks = await AsyncStorage.getItem(key);
            if (walks !== null) {

                const walksParsed = JSON.parse(walks)

                this.props.putWalks(walksParsed);
            }
        } catch (error) {
            console.log(error);

        }
    }

    componentDidMount() {
        // this._retrieveData('WALKS');
        this.props.fetchAllUsersKeepers();
        this.props.fetchAllOwnersOfUser();
        this.props.fetchWalksDB()
        setInterval(() => {
            this.refresh();
        }, 1000)

    }

    refresh = () => {
        this.goBack();
        this.goNext();
    }

    isToday = () => this.state.currentDay.format("DD-MM-YYYY") === this.state.today.format("DD-MM-YYYY");

    goToWalking = (id, type, ownerId) => {

        this.props.setCurrentWalk(id);
        this.props.setCurrentWalkId(id);
        this.props.createWalk(id, type, ownerId);
        this.props.navigation.navigate("Walking")
    }

    swapWalk = (swap) => {
         this.props.addSwap(swap); 
    }

    getPoo(walkId) {
        function isWalkById(walk) {
            return walk.id === walkId;
        }
        const currentWalk = this.props.walks.filter(isWalkById)[0];
        return currentWalk ? currentWalk.poo : 0;
    }

    canIReturn(walkId) {
        function isWalkById(walk) {
            return walk.id === walkId;
        }
        const currentWalk = this.props.walks.filter(isWalkById)[0];
        return currentWalk && currentWalk.swap ? true : false;
    }

    getStatus = (walkId) => {

        function isWalkById(walk) {
            return walk.id === walkId;
        }
        const currentWalk = this.props.walks.filter(isWalkById)[0];

        if (currentWalk && currentWalk.end) {
            return 'done'
        }

        if (currentWalk && currentWalk.start) {
            return 'inprogress'
        }

        return 'pending'
    }

    getSummaryTime = (walkId) => {
        function isWalkById(walk) {
            return walk.id === walkId;
        }
        const currentWalk = this.props.walks.filter(isWalkById)[0];

        const end = currentWalk && currentWalk.end;
        const start = currentWalk && currentWalk.start;

        const duration = moment.duration(moment(end).diff(moment(start)));
        const minutes = duration.asMinutes();

        return start ? end === 0 ? "" : `${Math.round(minutes)} min` : ""
    }

    findWalks = (currentId) => {

        function isEqualToCurrentId(walk) {
            return walk.id === currentId;
        }

        return this.props.walks.filter(isEqualToCurrentId);
    }

    getOwnerMorning = (currentId) => {
        const walk = this.findWalks(currentId);
        if (walk.length === 0) {

            const weekOfYear = moment(this.state.currentDay).add(1, 'days').week();


            if (this.props.orderOwner === 2) {
                if (!(weekOfYear % 2)) {
                    return YOU;
                } else {
                    return this.getCoOwner() ? this.getCoOwner() : YOU;
                }
            } else {
                if (!(weekOfYear % 2)) {
                    return this.getCoOwner() ? this.getCoOwner() : YOU;
                } else {
                    return YOU;
                }
            }


        } else {
            return (walk[0].owner === this.props.yourId) ? YOU : this.getCoOwner()
        }
    }

    getOwnerAfternoon = (currentId) => {
        const walk = this.findWalks(currentId);
        if (walk.length === 0) {

            const weekOfYear = moment(this.state.currentDay).add(1, 'days').week();

            if (this.props.orderOwner === 2) {
                if (weekOfYear % 2) {
                    return YOU;
                } else {
                    return this.getCoOwner() ? this.getCoOwner() : YOU;
                }
            } else {
                if (weekOfYear % 2) {
                    return this.getCoOwner() ? this.getCoOwner() : YOU;
                } else {
                    return YOU;
                }
            }
        }
        else {
            return (walk[0].owner === this.props.yourId) ? YOU : this.getCoOwner()
        }
    }

    getOwnerEvening = (currentId) => {
        const walk = this.findWalks(currentId);
        if (walk.length === 0) {
            const dayOfYear = moment(this.state.currentDay).dayOfYear();

            if (this.props.orderOwner === 2) {
                if (dayOfYear % 2) {
                    return YOU;
                } else {
                    return this.getCoOwner() ? this.getCoOwner() : YOU;
                }
            } else {
                if (dayOfYear % 2) {
                    return this.getCoOwner() ? this.getCoOwner() : YOU;
                } else {
                    return YOU;
                }
            }
        }
        else {
            return (walk[0].owner === this.props.yourId) ? YOU : this.getCoOwner()
        }

    }

    getOwnerOrCoOwnerId = (who) => {
        return who === YOU ? this.props.yourId : this.props.coownerId
    }

    getCoOwner = () => {
        return this.props.coowner ? this.props.coowner : null;
    }

    render() {
        const currentDayHash = this.state.currentDay.format("DDMMYYYY");
        const morningWalking = `${currentDayHash}001`;
        const afternoonWalking = `${currentDayHash}002`;
        const eveningWalking = `${currentDayHash}003`;

        const items = []
        if (this.state[EVENING]) {

            for (let i = 0; i < this.state[EVENING].poo; i++) {
                items.push(<FontAwesome5 name="poo" size={25} key={i} />)
            }
        }

        return (
            <Content>
                <Content padder>
                    <SwapHeader swap={this.props.swap} />
                    <WalkHeader
                        goBack={this.goBack}
                        goNext={this.goNext}
                        currentDay={this.state.currentDay} />


                    <WalkItem
                        title={"Poranny spacer"}
                        owner={this.getOwnerMorning(morningWalking)}
                        goTo={this.goToWalking}
                        id={morningWalking}
                        timeOfDay={MORNING}
                        poo={this.getPoo(morningWalking)}
                        canIReturn={this.canIReturn(morningWalking)}
                        status={this.getStatus(morningWalking)}
                        summaryTime={this.getSummaryTime(morningWalking)}
                        ownerId={this.getOwnerOrCoOwnerId(this.getOwnerMorning(morningWalking))}
                        isMine={this.props.yourId === this.getOwnerOrCoOwnerId(this.getOwnerMorning(morningWalking))}
                        swapWalk={this.swapWalk}
                        coownerId={this.props.coownerId}
                        currentDay={this.state.currentDay} />
                    <WalkItem
                        title={"Popołudniowy spacer"}
                        owner={this.getOwnerAfternoon(afternoonWalking)}
                        goTo={this.goToWalking}
                        id={afternoonWalking}
                        timeOfDay={AFTERNOON}
                        poo={this.getPoo(afternoonWalking)}
                        canIReturn={this.canIReturn(afternoonWalking)}
                        status={this.getStatus(afternoonWalking)}
                        summaryTime={this.getSummaryTime(afternoonWalking)}
                        ownerId={this.getOwnerOrCoOwnerId(this.getOwnerAfternoon(afternoonWalking))}
                        isMine={this.props.yourId === this.getOwnerOrCoOwnerId(this.getOwnerAfternoon(afternoonWalking))}
                        swapWalk={this.swapWalk}
                        coownerId={this.props.coownerId}
                        currentDay={this.state.currentDay} />
                    <WalkItem
                        title={"Wieczorny spacer"}
                        owner={this.getOwnerEvening(eveningWalking)}
                        goTo={this.goToWalking}
                        id={eveningWalking}
                        timeOfDay={EVENING}
                        poo={this.getPoo(eveningWalking)}
                        canIReturn={this.canIReturn(eveningWalking)}
                        status={this.getStatus(eveningWalking)}
                        summaryTime={this.getSummaryTime(eveningWalking)}
                        ownerId={this.getOwnerOrCoOwnerId(this.getOwnerEvening(eveningWalking))}
                        isMine={this.props.yourId === this.getOwnerOrCoOwnerId(this.getOwnerEvening(eveningWalking))}
                        swapWalk={this.swapWalk}
                        coownerId={this.props.coownerId}
                        currentDay={this.state.currentDay} />
                    {/* <Button block onPress={this.refresh}><FontAwesome name="refresh" size={25} color="white" /><Text>Odśwież</Text></Button> */}
                    {/* <Test /> */}
                    <Body><Text style={{ fontSize: 10 }}>Jesteś zalogowany jako {this.props.displayName}</Text></Body>
                    <Body><Text style={{ fontSize: 10 }}>{this.getCoOwner() ? `Jesteś połączony z ${this.getCoOwner()}` : `Nie jesteś z nikim połączony`}</Text></Body>
                    <Body><Text style={{ fontSize: 10 }}>v{app.expo.version}</Text></Body>
               
                </Content>
            </Content>
        );
    }
}

export default connect(
    ((state) => {


        function isWalkById(walk) {
            return walk.id === state.currentWalkId;
        }
        const currentWalk = state.walks.filter(isWalkById)[0];

        return ({
            walks: state.walks,
            displayName: state.user && state.user.user.email && state.user.user.email.split('@')[0],
            coowner: state.owners && state.owners[0] ? state.owners[0].email.split('@')[0] : null,
            coownerId: state.owners && state.owners[0] ? state.owners[0].id : null,
            yourId: state.user && state.user.user.uid,
            orderOwner: state.owners[0] ? state.owners[0].order : null,
            walk: currentWalk,
        })
    }), { ...actionCreators, ...actionCreatorsSetCurrentWalkId, ...keppersActionCreators, ...ownersActionCreators, ...swapActionCreators }
)(Walk);



