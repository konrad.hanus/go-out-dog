import { PUT_OWNER_TO_USER } from './action';

const initiaState = []

export function ownersReducer(state=initiaState, action){
    switch (action.type) {

        case PUT_OWNER_TO_USER:
          return action.payload;
    
        default:
          return state;
    };
   }
   
   export default ownersReducer;