export const PRESIST_AUTH = '@@LOGIN/PRESIST_AUTH';
export const REHYDRATE_AUTH = '@@LOGIN/REHYDRATE_AUTH';
export const SPINNER_OFF = '@@LOGIN/SPINNER_OFF';
export const SPINNER_ON = '@@LOGIN/SPINNER_ON';

export const actionCreators = {
    presistAuth: (payload) => ({
        type: PRESIST_AUTH, 
        payload
    }), 
    rehydrateAuth: (payload) => {
        return({
        type: REHYDRATE_AUTH,
        payload
    })},
    spinnerOff: () => {
        return({
        type: SPINNER_OFF,
        payload: false
    })},
    spinnerOn: () => {
        return({
        type: SPINNER_ON,
        payload: true
    })}
};