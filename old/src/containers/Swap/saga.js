import { takeEvery, call, all, fork, put, select } from 'redux-saga/effects';
import { ADD_SWAP, REMOVE_SWAP } from './action';
import { actionCreators as actionCreatorsCurrentWalkId } from '../CurrentWalkId/action';
import { actionCreators as actionCreatorsWalk } from '../Walk/action';
import { userId, coOwnerId, orderOwner } from '../../selectors/user';
import { currentWalkSelector, indexCurrentWalkSelector, walksSelector } from '../../selectors/walk';
// import { reduxSagaFirebase } from '../Walk/saga';


// function* onAddSwapSaga(action) {
//     const getUserId = yield select(userId);
//     yield call(reduxSagaFirebase.database.create, getUserId+'/swap', action.payload);
// }


function* onAddSwapSaga(action) {

    /** 
     * If current walk didn't exist then create new walk
     * 
     * */
    const { id, type, ownerId, color } = action.payload;
    yield put(actionCreatorsCurrentWalkId.setCurrentWalkId({ id: id }));

    let currentWalk = yield select(currentWalkSelector);

    let isNew = false;
    if (!currentWalk) {
        yield put(actionCreatorsWalk.createWalk({ id, type, ownerId }));
        currentWalk = yield select(currentWalkSelector);
        isNew = true;
    }

    const indexCurrentWalk = yield select(indexCurrentWalkSelector);
    const getUserId = yield select(userId);
    const getCoOwnerId = yield select(coOwnerId);

    let updateObject = {};

    /** 
     * If current walk belongs to me swap to belongs to coowner
     * 
     * */
    if (currentWalk.owner === getUserId) {
        updateObject.owner = getCoOwnerId;

        if (currentWalk.swap !== undefined && !isNew) {
             updateObject.swap = null
         }
         else {
            updateObject.swap = {
                oryginal: getUserId,
                data: new Date().toString()
            }
         }
    } else {
        updateObject.owner = getUserId;
        updateObject.swap = null
    }

    /** 
     * if is red ten permanently add swap 
     * 
     */
    if(color === "red")
    {
        updateObject.swap = {
            oryginal: getUserId,
            data: new Date().toString()
        }
    }


    /** 
     * Update walk in state
     * 
     * */

    const walk = {
        ...currentWalk,
        ...updateObject
    }
    yield put(actionCreatorsWalk.updateWalk({
        ...walk
    }, indexCurrentWalk));

    /** 
     * Save only one walk in firebase 
     * 
     * */

    yield put(actionCreatorsWalk.saveWalk(walk, walk.owner));
    if (!isNew) {
        const previousOwner = currentWalk.owner;
        yield put(actionCreatorsWalk.removeWalk(walk, previousOwner));
    }
}


function* onRemoveSwapSaga() {

    // const getUserId = yield select(userId);
    // let walks = yield call(reduxSagaFirebase.database.read,  getUserId+'/walks');

    // const getCoOwnerId = yield select(coOwnerId);
    // const getOrderOwner = yield select(orderOwner);

    // let ownersWalks = [];

    // if(getCoOwnerId){
    //     ownersWalks = yield call(reduxSagaFirebase.database.read,  getCoOwnerId+'/walks');
    // }
    // if(!ownersWalks)
    // {
    //     ownersWalks = [];
    // }

    // if(!walks)
    // {
    //     walks = []
    // }
    // let concatWalks = walks.concat(ownersWalks);

    // if(getOrderOwner === 2)
    // {
    //     concatWalks = ownersWalks.concat(walks);
    // }

    // yield put(actionCreators.putWalks(concatWalks));

}


function* addSwapSaga() {
    yield takeEvery(ADD_SWAP, onAddSwapSaga);
}

function* removeSwapSaga() {
    yield takeEvery(REMOVE_SWAP, onRemoveSwapSaga);
}

export function* swapSaga() {
    yield all([fork(addSwapSaga), fork(removeSwapSaga)]);
}
