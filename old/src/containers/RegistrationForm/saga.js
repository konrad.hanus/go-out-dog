import { takeEvery, call, put, all, fork } from 'redux-saga/effects';
import { SIGNUP, LOGIN, LOGOUT, actionCreators } from './action';
import { reduxSagaFirebase } from '../Walk/saga';
import {actionCreators as actionCreatorsLogin } from './../LogIn/action';
import {userId, userEmail} from '../../selectors/user';

export function* onSignUpSaga(action) {
	const email = action.payload.email;
	const password = action.payload.password;
	const username = action.payload.username;
	const redirect = action.payload.redirect;
	try {
		const user = yield call(reduxSagaFirebase.auth.createUserWithEmailAndPassword, email, password);

		const getUserId = user.user.uid;
		const getUserEmail = user.user.email;

		yield call(reduxSagaFirebase.database.update, 'users/'+getUserId, {id: getUserId, email: getUserEmail});
		yield call(reduxSagaFirebase.database.update, getUserId, {id: getUserId, email: getUserEmail});
		yield call(action.payload.redirect, "LogIn"); 

		//redirect to login page
		// zaloguj
	} catch (error) {
		yield call(alert, 'Takie konto juz istnieje. Spróbuje się zalogować. ', error);
	}
	yield put(actionCreators.login({ email, password, redirect }));
}

export function* onloginSaga(action) {
	const email = action.payload.email;
	const password = action.payload.password;
	yield put(actionCreatorsLogin.spinnerOn());
	try {
		
		const data = yield call(reduxSagaFirebase.auth.signInWithEmailAndPassword, email, password); 
		yield put(actionCreators.loginSuccess(data));

		

		yield put(actionCreatorsLogin.presistAuth({email, password}));
		// yield call(action.payload.redirect,"Home");
		yield call(action.payload.redirect,"Walk");
		yield put(actionCreatorsLogin.spinnerOff());
		
	} catch (error) {
		yield call(alert, error);
		yield put(actionCreatorsLogin.spinnerOff());
	}
}

export function* onlogOutSaga(action) {
	
	try {
		const data = yield call(reduxSagaFirebase.auth.signOut);

		yield call(action.payload, "LogOut");
		
		
	} catch (error) {
		yield call(alert, error);
	}
}

export function* signUpSaga() {
	yield takeEvery(SIGNUP, onSignUpSaga);
}

export function* loginSaga() {
	yield takeEvery(LOGIN, onloginSaga);
}

export function* logOutSaga() {
	yield takeEvery(LOGOUT, onlogOutSaga);
}


export function* registrationFormSaga() {
	yield all([ fork(signUpSaga), fork(loginSaga), fork(logOutSaga) ]);
}
