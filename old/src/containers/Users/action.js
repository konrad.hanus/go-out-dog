export const PUT_ALL_USERS = '@@USERS/PUT_ALL_USERS';

export const actionCreators = {
    putAllUsers: (allUsers) => {
        return ({
            type: PUT_ALL_USERS,
            payload: allUsers
        })
    }
}