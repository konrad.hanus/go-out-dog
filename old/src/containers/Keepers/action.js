export const ADD_KEEPERS_TO_USER = '@@KEEPERS/ADD_TO_USER';
export const PUT_ALL_USERS_KEEPERS = '@@KEEPERS/PUT_ALL_USERS_KEEPERS';
export const FETCH_ALL_USERS_KEPPERS = '@@KEEPERS/FETCH_ALL_USERS_KEPPERS';
export const REMOVE_USERS_KEEPER = '@@KEEPERS/REMOVE_USERS_KEEPER';

export const actionCreators = {
    addKeepersToUser: (keeper) => {
        return ({
            type: ADD_KEEPERS_TO_USER,
            payload: keeper
        })
    },
    fetchAllUsersKeepers: () => {
        return ({
            type: FETCH_ALL_USERS_KEPPERS,
        })
    },
    putAllUsersKeepers: (allUsersKeppers) => {
        return ({
            type: PUT_ALL_USERS_KEEPERS,
            payload: allUsersKeppers
        })
    },
    removeUsersKeeper: (keeepr) => {
        return ({
            type: REMOVE_USERS_KEEPER,
            payload: keeepr
        })
    },
    
};