import { takeEvery, call, put, all, fork, select } from 'redux-saga/effects';
import { ADD_KEEPERS_TO_USER, FETCH_ALL_USERS_KEPPERS, REMOVE_USERS_KEEPER } from './action';
import { actionCreators as usersActionCreators} from '../Users/action';
import { actionCreators } from './action';
import { reduxSagaFirebase } from '../Walk/saga';
import { userId } from '../../selectors/user';

function* onAddKeepersToUserSaga(action) {
    const getUserId = yield select(userId);
    const keeper = action.payload;
    yield call(reduxSagaFirebase.database.update, getUserId+'/keepers/'+keeper.id, keeper);
    yield put(actionCreators.fetchAllUsersKeepers());
}

function* onRemoveKeepersFromUserSaga(action) {
    const getUserId = yield select(userId);
    const keeper = action.payload;
    yield call(reduxSagaFirebase.database.delete, getUserId+'/keepers/'+keeper.id);
    yield put(actionCreators.fetchAllUsersKeepers());
}

function* onFetchAllUsersKeepersSaga() {
    const getUserId = yield select(userId);
    const allUsersKeppers = yield call(reduxSagaFirebase.database.read, getUserId+'/keepers/');

    if (allUsersKeppers) {
        const allUsersKeepersArray = Object.values(allUsersKeppers);
        yield put(actionCreators.putAllUsersKeepers(allUsersKeepersArray));
    }else{
        yield put(actionCreators.putAllUsersKeepers([]));
    }
}

export function* addKeepersToUserSaga() {
    yield takeEvery(ADD_KEEPERS_TO_USER, onAddKeepersToUserSaga);
}

export function* fetchAllUsersKeepersSaga() {
    yield takeEvery(FETCH_ALL_USERS_KEPPERS, onFetchAllUsersKeepersSaga);
}

export function* removeKeepersFromUserSaga() {
    yield takeEvery(REMOVE_USERS_KEEPER, onRemoveKeepersFromUserSaga);
}

export function* keppersSaga() {
    yield all([fork(addKeepersToUserSaga), fork(fetchAllUsersKeepersSaga), fork(removeKeepersFromUserSaga)]);
}
