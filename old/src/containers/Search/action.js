export const FETCH_ALL_USERS = '@@SEARCH/FETCH_ALL_USERS';


export const actionCreators = {
    fetchAllUsers: () => {
        return ({
            type: FETCH_ALL_USERS,
        })
    },
}