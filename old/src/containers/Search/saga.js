import { takeEvery, call, put, all, fork, select } from 'redux-saga/effects';
import { FETCH_ALL_USERS } from './action';
import { actionCreators as usersActionCreators} from '../Users/action';
import { reduxSagaFirebase } from '../Walk/saga';
import { userId } from '../../selectors/user';

function* onFetchAllUsersSaga() {
    const getUserId = yield select(userId);
    const allUsers = yield call(reduxSagaFirebase.database.read, '/users');

    if (allUsers) {
        const allUsersArray = Object.values(allUsers);
        yield put(usersActionCreators.putAllUsers(allUsersArray));
    }
}
export function* fetchAllUsersSaga() {
    yield takeEvery(FETCH_ALL_USERS, onFetchAllUsersSaga);
}

export function* searchSaga() {
    yield all([fork(fetchAllUsersSaga)]);
}
