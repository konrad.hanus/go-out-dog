import React from 'react';
import { Body, Button, Container, Content, Drawer, Header, Icon, Left, Tab, Tabs, Text, Title } from 'native-base';

import { connect } from 'react-redux';
import { actionCreators } from './../RegistrationForm/action';


export class LogOut extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			language: [ 'Polish', 'English' ]
		};
	}

	componentDidMount(){
		this.props.loginSuccess(null);
	}

	render() {
		return (
			<Container>
				<Container style={{ alignItems: 'center', justifyContent: 'center', flex: 5}}>
					<Text>Zostałeś pomyślnie wylogowany</Text>
				</Container>
				<Container style={{ alignItems: 'center', justifyContent: 'center', flex: 2}}>
					<Button block success onPress={() => this.props.navigation.navigate('LogIn')}><Text>Zaloguj się ponownie</Text></Button>
					<Text>{" "}</Text>
					<Button block onPress={() => this.props.navigation.navigate('RegistrationForm')}><Text>Zarejestruj się</Text></Button>
				</Container>
				<Container style={{ alignItems: 'center', justifyContent: 'center', flex: 2}}>
					
				</Container>
			</Container>
		);
	}
}

export default connect(null, actionCreators)(LogOut);
