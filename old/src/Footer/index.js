import React, { Component } from 'react';
import { Footer, FooterTab, Button, Icon, Text, Badge } from 'native-base';
import IconFoundation from 'react-native-vector-icons/Foundation';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Link } from "react-router-native";
MaterialCommunityIcons
export default class FooterTabsBadgeExample extends Component {
   
    render() {
        const currentPage = this.props.current;
        return (
            <Footer>
                <FooterTab>
                    <Button active={currentPage === "Walk" ? true : false } badge vertical onPress={() => this.props.navigation.navigate("Walk")}>
                        {/* <Badge><Text>2</Text></Badge> */}
                        <IconFoundation name="guide-dog" size={30} />
                        <Text style={{fontSize: 10}}>Spacery</Text>
                    </Button>
                    <Button active={currentPage === "Points" ? true : false } vertical onPress={() => this.props.navigation.navigate("Points")}>
                        <IconFontAwesome name="star" size={30} />
                        <Text style={{fontSize: 10}}>Punkty</Text>
                    </Button>
                    <Button active={currentPage === "DogSummary" ? true : false } badge vertical onPress={() => this.props.navigation.navigate("DogSummary")}>
                        <MaterialCommunityIcons name="dog" size={30} />
                        <Text style={{fontSize: 10}}>Pies</Text>
                    </Button> 
                    <Button active={currentPage === "Swap" ? true : false } badge vertical onPress={() => this.props.navigation.navigate("Swap")}>
                         {/* <Badge ><Text>51</Text></Badge> */}
                        <IconFontAwesome name="handshake-o" size={30} />
                        <Text style={{fontSize: 10}}>Zamiana</Text>
                    </Button>

                    {/* <Button active={currentPage === "LogIn" ? true : false } badge vertical onPress={() => this.props.navigation.navigate("LogIn")}>
                        <IconFontAwesome name="login" size={30} />
                        <Text>Logowanie</Text>
                    </Button>  */}
                    {/* <Button active={currentPage === "Keepers" ? true : false } vertical onPress={() => this.props.navigation.navigate("Keepers")}>
                        <IconFontAwesome5 name="user-friends" size={25} />
                        <Text>Opiekuni</Text>
                    </Button> */}

                     <Button active={currentPage === "Friends" ? true : false } vertical onPress={() => this.props.navigation.navigate("Friends")}>
                        <IconFontAwesome5 name="user-friends" size={25} />
                        <Text style={{fontSize: 10}}>Znajomi</Text>
                    </Button>

                    {/* <Button active={currentPage === "Settings" ? true : false } badge vertical onPress={() => this.props.navigation.navigate("Settings")}>
                        <IconFontAwesome name="cog" size={30} />
                        <Text>Ustawienia</Text>
                    </Button>  */}
                    
                     {/* <Button active={currentPage === "LogOut" ? true : false } badge vertical onPress={() => this.props.navigation.navigate("LogOut")}>
                        <IconFontAwesome name="power-off" size={30} />
                        <Text>Wyloguj</Text>
                    </Button>  */}

                </FooterTab>
            </Footer >
        );
    }
}