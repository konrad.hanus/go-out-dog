import React, { Component } from 'react';
import { Card, CardItem, Text, Icon, Button, Left, Right, Body, Content, Grid, Col, View } from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
moment.locale('pl');
class WalkHeader extends Component {

    constructor(props) {
        moment.locale('pl');
        super(props)
        this.state = {
            today: moment(),

        }
    }
    isToday = () => this.props.currentDay.format("DD-MM-YYYY") === this.state.today.format("DD-MM-YYYY");

    goToWalking = (id, type, owner) => {

        this.props.setCurrentWalk(id);
        this.props.createWalk({
            id,
            type,
            owner
        });
        this.props.navigation.navigate("Walking")
    }

    daysMap = (day) => {
        switch (day) {
            case 'Monday':
                return 'Poniedziałek';
            case 'Tuesday':
                return 'Wtorek';
            case 'Wednesday':
                return 'Środa';
            case 'Thursday':
                return 'Czwartek';
            case 'Friday':
                return 'Piątek';
            case 'Saturday':
                return 'Sobota';
            case 'Sunday':
                return 'Niedziela';
            default:
                return day
        }
    }

    render() {
        return (
                <Card>
                    <CardItem header>
                        <Left>
                            <Button small onPress={() => this.props.goBack()} small primary>
                                <Icon name="arrow-back" />
                            </Button>
                        </Left>
                        <Body>
                            <Text style={this.isToday() ? { paddingTop: 5, fontWeight: 'bold' } : { paddingTop: 5 }}>{this.daysMap(this.props.currentDay.locale('pl').format("dddd"))}</Text>
                            <Text style={this.isToday() ? { paddingTop: 5, fontWeight: 'bold' } : { paddingTop: 5 }}>
                                {this.props.currentDay.format("DD-MM-YYYY")}
                            </Text>
                        </Body>
                        <Right>
                            <Button small onPress={() => this.props.goNext()} small primary>
                                <Icon name="arrow-forward" />
                            </Button>
                        </Right>
                    </CardItem>
                </Card>
        );
    }
}

export default WalkHeader;



