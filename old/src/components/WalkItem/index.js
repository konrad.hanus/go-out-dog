import React, { PureComponent } from 'react';
import { Card, CardItem, Text, Icon, Button, Left, Right, Badge, Body } from 'native-base';
import moment from 'moment';
import Fontisto from 'react-native-vector-icons/Fontisto';
import { AsyncStorage } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { YOU } from './../../containers/Walk/';

const MORNING = 'morinig';
const AFTERNOON = 'afternoon';
const EVENING = 'evening';
moment.locale('pl');
class WalkItem extends PureComponent {

    constructor(props) {
        super(props)
        this.state = {
            currentDay: moment(),
            today: moment(),
            [MORNING]: null,
            [AFTERNOON]: null,
            [EVENING]: null
        }
    }

    getbackgroundColor = () => {
        switch (this.props.status) {
            case 'inprogress':
                return 'yellow';

            case 'pending':
                return 'white';

            case 'done':
                return 'silver';
            default:
                return 'white'
        }
    }

    getActionText = () => {
        switch (this.props.status) {
            case 'inprogress':
                return 'Kontynuuj';

            case 'pending':
                return 'Idę';

            case 'done':
                return 'Zobacz';
            default:
                return 'white'
        }
    }

    isVisibleSwapButton = () => {
        if (this.props.status === 'inprogress' || this.props.status === 'done') {
            return false;
        }
        return true;
    }

    getOwner = () => {
        switch (this.props.owner) {
            case YOU:
                return <Badge style={{ backgroundColor: 'blueviolet' }}><Text>{this.props.owner}</Text></Badge>;

            case this.props.owner:
                return <Badge primary><Text>{this.props.owner}</Text></Badge>;

            default:
                return <Badge><Text>{this.props.owner}</Text></Badge>;
        }
    }

    isActive = () => {
        return this.props.currentDay <= this.state.today
    }

    render() {

        const items = []

        for (let i = 0; i < this.props.poo; i++) {
            items.push(<FontAwesome5 name="poo" size={25} key={i} />)
        }
        
        return (
            <Card >
                <CardItem header style={{ backgroundColor: this.getbackgroundColor() }}>
                    <Left>{this.getOwner()}</Left>

                </CardItem>
                <CardItem style={{ backgroundColor: this.getbackgroundColor() }}>
                    <Left>
                        {this.props.isMine && <Button small iconLeft success block 
                        disabled={!this.isActive()}
                        onPress={() => this.props.goTo({
                            id: this.props.id,
                            type: this.props.timeOfDay,
                            ownerId: this.props.ownerId,
                        })

                        }>
                            <Icon name='paw' />
                            <Text>{this.getActionText()}</Text>
                        </Button>}
                    </Left>
                    <Right>
                        {this.isVisibleSwapButton() && this.props.coownerId && (this.props.isMine ?
                            
                            
                            this.props.canIReturn ? 
                            <Button small iconRight warning block
                                onPress={() => this.props.swapWalk({
                                    id: this.props.id,
                                    type: this.props.timeOfDay,
                                    ownerId: this.props.ownerId,
                                    color: "yellow"
                                })

                                }>
                                <Fontisto name='arrow-swap' style={{ paddingLeft: 15, fontSize: 20, color: 'white' }} />
                                <Text>Odrzuć (-1) </Text>
                            </Button>
                            :
                            
                            <Button small iconRight danger block
                                onPress={() => this.props.swapWalk({
                                    id: this.props.id,
                                    type: this.props.timeOfDay,
                                    ownerId: this.props.ownerId,
                                    color: "red"
                                })   

                                }>
                                <Fontisto name='arrow-swap' style={{ paddingLeft: 15, fontSize: 20, color: 'white' }} />
                                <Text>Zamiana (-1) </Text>
                            </Button>
                            :  <Button small iconRight success block onPress={() => this.props.swapWalk({
                                    id: this.props.id,
                                    type: this.props.timeOfDay,
                                    ownerId: this.props.ownerId,
                                    color: "green"
                                })
    
                                }>
                                    <Fontisto name='arrow-swap' style={{ paddingLeft: 15, fontSize: 20, color: 'white' }} />
                                    <Text>Zamiana (+1)</Text>
                                </Button>)
                            
                        }
                    </Right>
                </CardItem>
                <CardItem footer style={{ backgroundColor: this.getbackgroundColor() }}>
                    <Body><Text>{this.props.title}</Text></Body>
                    <Right>
                        <Text>{items} {this.props.summaryTime}</Text>
                    </Right>
                </CardItem>
            </Card>
        );
    }
}

export default WalkItem

