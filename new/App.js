import React, { useState, useRef } from "react";
// import { StatusBar } from "expo-status-bar";
import { Text, View, StyleSheet } from "react-native";
import Fiber from "./src/pages/Fiber";
// import Map2 from "./src/component/Map2";
import { NativeBaseProvider } from "native-base";
// import ProfileAvatar from "./src/component/ProfileAvatar";
// import Menu from "./src/component/Menu";
// import Bag from "./src/component/Bag";
// import { PanGestureHandler } from "react-native-gesture-handler";
// import Gestures from "./src/component/Gestures";
// import MapView, { Circle, Polygon } from "react-native-maps";
// import Pin from "./src/component/Pin";
// import Doge from "./src/component/Pin/Doge.jpeg";
// import LargeMap from "./src/component/LargeMap";
// import SCALE from "./src/helpers/scale";
import { Provider } from "react-redux";
import configStore, { sagaMiddleware } from "./store";
import initSagas from "./initSagas";
import HomePage from "./src/pages/HomePage";
import WelcomePage from "./src/pages/WelcomePage";
import SignInPage from "./src/pages/SignInPage";
import RegisterPage from "./src/pages/RegisterPage";
import { NavigationContainer, DefaultTheme } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

// import { connect } from "react-redux";
// import { actionCreators } from "./src/containers/MyLocation/action";
import DogApp from "./src/containers/DogApp";
// import ThreeJs from "./src/component/ThreeJs";
// import PlayerAvatar from './src/component/Molecules/PlayerAvatar';
// import ProfileAvatar from './src/component/ProfileAvatar';
// import DogProfile from "./src/component/Molecules/DogProfile/indexNew";

import AndroidLocation from "./src/component/Atoms/AndroidLocation";
import PlayerAvatar from "./src/component/Molecules/PlayerAvatar";
import reduxSaga from "redux-saga";
import WizardPage from "./src/pages/Wizard";
import WizardDogs from "./src/pages/WizardDogs";
import WizardYourDogPage from "./src/pages/WizardYourDog";
import PAGE from "./src/pages/pages";

export const store = configStore();
initSagas(sagaMiddleware);

// Settings for disabling logging in three
window.log = window.console.log;
window.console.log = () => {};
// delete global._WORKLET_RUNTIME;
// delete global.WebGLRenderingContext;
const Stack = createNativeStackNavigator();

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "rgb(255, 45, 85)",
    background: "white",
  },
};

export default App = (props) => {
  console.log("test");
  const distance = 0;
  return (
    <Provider store={store}>
      <NavigationContainer theme={MyTheme}>
        <NativeBaseProvider>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}
          >
            <Stack.Screen name={PAGE.HOME} component={HomePage} />
            <Stack.Screen name={PAGE.WIZARD} component={WizardPage} />
            <Stack.Screen name={PAGE.WIZARDDOGS} component={WizardDogs} />
            <Stack.Screen
              name={PAGE.WIZARDYOURDOG}
              component={WizardYourDogPage}
            />
            <Stack.Screen name={PAGE.WELCOME} component={WelcomePage} />
            <Stack.Screen name={PAGE.SIGNIN} component={SignInPage} />
            <Stack.Screen name={PAGE.REGISTER} component={RegisterPage} />
            <Stack.Screen name={PAGE.PERMISSION} component={AndroidLocation} />

            <Stack.Screen name={PAGE.GAME} component={DogApp} />
          </Stack.Navigator>
          {/* <DogApp /> */}
        </NativeBaseProvider>
      </NavigationContainer>
    </Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
