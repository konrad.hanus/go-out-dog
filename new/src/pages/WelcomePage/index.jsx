import { Text, View, StyleSheet } from "react-native";
import { Center } from "native-base";
import Page from "../Page";
import IconBack from "../../component/Atoms/IconBack";
import Logo from "../../component/Atoms/LogoImage";
import WelcomePageButtons from "../../component/Molecules/WelcomePageButtons";
import PAGE from "../pages";

const WelcomePage = ({ navigation }) => {
  return (
    <Page backPage={PAGE.HOME} navigation={navigation}>
      <Center w="100%" h="100%">
        <View style={styles.imageWithIcon}>
          <Logo width={259} height={259}/>
        </View>
        <Text style={styles.text}>Dołącz do gry</Text>
        <WelcomePageButtons navigation={navigation}/>
      </Center>
    </Page>
  );
};

export default WelcomePage;

const styles = StyleSheet.create({
  imageWithIcon: {
    flexDirection: "row",
    width: "85%",
  },
  text: {
    fontSize: 22,
    fontWeight: "400",
    lineHeight: 47,
  },
});
