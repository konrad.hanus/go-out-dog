import { SafeAreaView } from 'react-native-safe-area-context';
import { ScrollView } from "native-base";
import BackButton from "../../component/Molecules/BackButton";

const Page = ({children, isHome, backPage, navigation}) => 
<SafeAreaView>
    <ScrollView w="100%" h="100%">
        {!isHome && <BackButton navigation={navigation} page={backPage} />}
        {children}
        </ScrollView>
</SafeAreaView>;
export default Page;