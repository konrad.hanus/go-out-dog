import React from "react";
import { Input } from "native-base";
import { useDispatch } from "react-redux";
import { actionCreators } from "../../../containers/Registration/action";

const DogInput = (props) => {
  const dispatch = useDispatch();

  const onDogNameChange = (dogName) => {
    dispatch(actionCreators.changeDogName(dogName));
  };

  return (
    <Input
      variant="rounded"
      placeholder={props.placeholder}
      width="80%"
      height={20}
      borderRadius={30}
      marginTop={2}
      size={16}
      value={props.dogName}
      onChangeText={onDogNameChange}
    />
  );
};

export default DogInput;
