import Page from "../Page";
import { Text, View, StyleSheet } from "react-native";
import React from "react";
import male from "../Wizard/assets/male.png";
import female from "../Wizard/assets/female.png";
import PAGE from "./../../pages/pages";
import GenderContainer from "../Wizard/GenderContainer";
import SliderDogs from "./SliderDogs";
import DogInput from "./DogInput";
import NextButton from "./NextButton";
import TextContainer from "./TextContainer";
import { useDispatch, useSelector } from "react-redux";
import { actionCreators } from "../../containers/Registration/action";

export default WizardDogsPage = ({ navigation }) => {
  const dogName = useSelector((state) => state.registration.dogName);
  const dispatch = useDispatch();

  return (
    <Page navigation={navigation} backPage={PAGE.WIZARD}>
      <SliderDogs />
      <View style={styles.center}>
        <DogInput placeholder="Nazwa" dogName={dogName} />
      </View>
      <View style={styles.genderContainer}>
        <GenderContainer
          source={male}
          onPress={() => dispatch(actionCreators.changeDogGenderMale())}
        />
        <GenderContainer
          source={female}
          onPress={() => dispatch(actionCreators.changeDogGenderFemale())}
        />
      </View>
      <NextButton onPress={() => navigation.navigate(PAGE.WIZARDYOURDOG)} />
      <TextContainer />
    </Page>
  );
};

const styles = StyleSheet.create({
  center: {
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
  },
  genderContainer: {
    marginTop: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
});
