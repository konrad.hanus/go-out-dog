import React, { Suspense } from "react";
import Carousel from "react-native-reanimated-carousel";
import { View, Dimensions } from "react-native";
import { Vector3 } from "three";
import Setup from "../../../component/Molecules/Setup";
import Szarik from "../../../component/Atoms/Szarik";
import Pinczer from "../../../component/Atoms/Pinczer";
import Chihuahua from "../../../component/Atoms/Chihuahua";
import GreenDog from "../../../component/Atoms/GreenDog";
import ErrorBoundary from "../../../component/Atoms/ErrorBoundaries";
import Loading from "../../WizardYourDog/Loading";
import { actionCreators } from "../../../containers/Registration/action";
import { useDispatch } from "react-redux";
import { DOGTYPE } from "../DogsData";

const SliderDogs = () => {
  const width = Dimensions.get("window").width;
  const height = Dimensions.get("window").width - 100;

  const dispatch = useDispatch();

  const strategyPatternSelectDog = (index) => {
    switch(index){
      case 0: 
        return dispatch(actionCreators.changeDogType(DOGTYPE.Szarik));
      case 1: 
        return dispatch(actionCreators.changeDogType(DOGTYPE.Pinczer));
      case 2: 
        return dispatch(actionCreators.changeDogType(DOGTYPE.Chihuahua));
      default:
        return dispatch(actionCreators.changeDogType(DOGTYPE.GreenDog));
    }
  }

  return (
    <View style={{ flex: 1 }}>
      {/* on android the carousel is not working */}
      <Carousel
        loop
        width={width}
        height={height}
        autoPlay={false}
        data={[...new Array(4).keys()]}
        scrollAnimationDuration={1000}
        onSnapToItem={(index) => strategyPatternSelectDog(index)}
        renderItem={({ index }) => (
          <View
            style={{
              flex: 1,
              borderWidth: 0,
              justifyContent: "center",
            }}
          >
            <Setup
              cameraFov={50}
              cameraPosition={new Vector3(0, 0.7, 1)}
              far={1000}
            >
              <Suspense fallback={Loading}>
                {index === 0 && (
                  <>
                    <Szarik
                      key={"Szarik-3D"}
                      position={[0, -0.15, 0]}
                      rotation={[0, 0.7, 0]}
                      scale={[0.7, 0.7, 0.7]}
                      animation={1}
                      isRotation={false}
                    />
                  </>
                )}
                {index === 1 && (
                  <>
                    <ErrorBoundary>
                      <Pinczer
                        key={"Pinczer-3D"}
                        position={[0, -0.15, 0]}
                        rotation={[0, 0.7, 0]}
                        scale={[0.7, 0.7, 0.7]}
                        animation={1}
                        isRotation={false}
                      />
                    </ErrorBoundary>
                  </>
                )}
                {index === 2 && (
                  <>
                    <Chihuahua
                      key={"Chihuahua-3D"}
                      position={[0, -0.15, 0]}
                      rotation={[0, 0.7, 0]}
                      scale={[1, 1, 1]}
                      animation={1}
                      isRotation={false}
                    />
                  </>
                )}
                {index === 3 && (
                  <>
                    <GreenDog
                      key={"Green-3D"}
                      position={[0, -0.15, 0]}
                      rotation={[0, 0.7, 0]}
                      scale={[0.7, 0.7, 0.7]}
                      animation={1}
                      isRotation={false}
                    />
                  </>
                )}
              </Suspense>
            </Setup>
          </View>
        )}
      />
    </View>
  );
};

export default SliderDogs;
