import React from "react";
import { Text, View, StyleSheet } from "react-native";
import { Link } from "native-base";

const TextContainer = () => {
  const link = "https://www.schroniskowroclaw.pl/adopcja/psy-do-adopcji-c2";

  return (
    <View style={styles.textContainer}>
      <Text style={styles.text}>Nie masz psa?</Text>
      <Link href={link}>
        <Text style={styles.text}>Odwiedź schronisko Wrocławskie</Text>
      </Link>
    </View>
  );
};

export default TextContainer;

const styles = StyleSheet.create({
  textContainer: {
    marginTop: 30,
    marginBottom: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    margin: 5,
    fontWeight: "400",
    lineHeight: 19,
    fontSize: 19,
    textAlign: "center",
  },
});
