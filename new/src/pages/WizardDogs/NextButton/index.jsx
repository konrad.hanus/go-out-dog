import React from "react";
import { View, StyleSheet } from "react-native";
import HomeButton from "../../../component/Atoms/HomeButton";
import PAGE from "../../pages";

const NextButton = ({ onPress }) => (
  <View style={styles.center}>
    <HomeButton
      width="50%"
      height={10}
      onPress={onPress}
      //   onPress={() => navigation.navigate(PAGE.WIZARDYOURDOG)}
      title="Dalej"
    />
  </View>
);

export default NextButton;

const styles = StyleSheet.create({
  center: {
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
  },
});
