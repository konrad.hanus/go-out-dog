import React from "react";
import Page from "../Page";
import SignInDivider from "../../component/Atoms/SignInDivider";
import NavLogo from "../../component/Molecules/SignInNavLogo";
import SignInTitle from "../../component/Atoms/SignInTextTitle";
import SignInTextContent from "../../component/Atoms/SignInTextContent";
import SignInInputs from "../../component/Molecules/SignInInputs";
import SignInTextRecover from "../../component/Atoms/SignInTextRecover";
import SignInButton from "../../component/Atoms/SignInButton";
import SignInSocialImages from "../../component/Atoms/SigInSocialImages";

const SignInPage = ({ navigation }) => {
  const alertHelp = () => alert("Przekierowanie do pomocy...");
  const alertSignIn = () => alert("Logowanie w toku...");
  const onClickGoToRegisterPage = () => navigation.push("Register");

  return (
      <Page isHome={true}>
        <NavLogo navigation={navigation} />
        <SignInTitle title="Logowanie" />
        <SignInTextContent
          contentNormal="Jeżeli potrzebujesz pomocy"
          contentLink="Kliknij tutaj"
          navigation={navigation}
          onPressLink={alertHelp}
        />
        <SignInInputs />
        <SignInTextRecover title="Odzyskaj hasło" />
        <SignInButton title="Zaloguj się" onPress={alertSignIn} />
        <SignInDivider title="Lub" />
        <SignInSocialImages />
        <SignInTextContent
          contentNormal="Nie masz konta?"
          contentLink="Załóż konto"
          navigation={navigation}
          onPressLink={onClickGoToRegisterPage}
        />
      </Page>
  );
};

export default SignInPage;
