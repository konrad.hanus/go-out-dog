import React from "react";
import Page from "../Page";
import SignInDivider from "../../component/Atoms/SignInDivider";
import NavLogo from "../../component/Molecules/SignInNavLogo";
import SignInTitle from "../../component/Atoms/SignInTextTitle";
import SignInTextContent from "../../component/Atoms/SignInTextContent";
import SignInInputs from "../../component/Molecules/SignInInputs";
import SignInButton from "../../component/Atoms/SignInButton";
import SignInSocialImages from "../../component/Atoms/SigInSocialImages";
import { useSelector } from 'react-redux';


const RegisterPage = ({ navigation }) => {
  const alertHelp = () => alert("Przekierowanie do pomocy...");
  const onClickWizard = () => navigation.push("Wizard");
  const onClickGoToSignInPage = () => navigation.push("SignIn");
  const email = useSelector(state=>state.registration.email);
  const nick = useSelector(state=>state.registration.nick);
  const password= useSelector(state=>state.registration.password);
  return (
      <Page isHome={true}>
        <NavLogo navigation={navigation} />
        <SignInTitle title="Rejestracja" />
        <SignInTextContent
          contentNormal="Jeżeli potrzebujesz pomocy"
          contentLink="Kliknij tutaj"
          navigation={navigation}
          onPressLink={alertHelp}
        />
        <SignInInputs 
        isNameInputExist={true} 
        email={email}
        nick={nick}
        password={password}
        />
        <SignInButton title="Załóż konto" onPress={onClickWizard} />
        <SignInDivider title="Lub" />
        <SignInSocialImages />
        <SignInTextContent
          contentNormal="Masz konto?"
          contentLink="Zaloguj się"
          navigation={navigation}
          onPressLink={onClickGoToSignInPage}
        />
      </Page>
  );
};

export default RegisterPage;
