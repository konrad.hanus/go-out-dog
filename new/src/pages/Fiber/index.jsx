import React, { Suspense } from 'react'
import { useGLTF } from '@react-three/drei/native'
import { Canvas } from '@react-three/fiber/native'
import DogGLB from '../../assets/dog.glb'
import PinczerGLB from '../../assets/pinczermove.glb';
import ZenekGLB from '../../assets/mananimation.glb';
import SzarikGLB from '../../assets/szarikmove.glb';
import ChihuahuaGLB from '../../assets/chihuahua.glb';

function Dog(props) {
  const { scene, animations } = useGLTF(DogGLB);
  
  console.log(animations);
  return <primitive {...props} object={scene} />
}

function Zenek(props) {
    const { scene } = useGLTF(ZenekGLB)
    return <primitive {...props} object={scene} />
  }

  function Szarik(props) {
    const { scene } = useGLTF(SzarikGLB)
    return <primitive {...props} object={scene} />
  }


  function Pinczer(props) {
    const { scene } = useGLTF(PinczerGLB)
    return <primitive {...props} object={scene} />
  }

  function Chihuahua(props) {
    const { scene } = useGLTF(ChihuahuaGLB)
    return <primitive {...props} object={scene} />
  }

 const Loading = () => <Text>ładowanie</Text>

export default function App() {
  return (
    <Canvas frameloop={'always'}>
      <Suspense fallback={Loading}>
      <ambientLight />
        <Dog      position={[0.5,0,0]}/>
        <Zenek    
          position={[0,0,0]} 
        />
        <Szarik   position={[-0.5,0,0]}/>
        <Pinczer    position={[-1,0,0]}/>
        <Chihuahua  position={[1,0,0]}/>
      </Suspense>
    </Canvas>
  )
}