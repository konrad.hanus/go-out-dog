import Page from "../Page";
import { View, Dimensions } from "react-native";
import React from "react";
import Carousel from "react-native-reanimated-carousel";
import PAGE from "./../../pages/pages";
import Male3D from "./Male3D";
import Female3D from "./Female3D";
import NextButton from "../WizardDogs/NextButton";
import { useDispatch, useSelector } from "react-redux";
import { TouchableHighlight } from "react-native";
import { actionCreators } from "../../containers/Registration/action";
import ErrorBoundary from "../../component/Atoms/ErrorBoundaries";

export default WizardPage = ({ navigation }) => {
  const width = Dimensions.get("window").width;
  const height = Dimensions.get("window").height - 100;

  const dispatch = useDispatch();

  return (
    <Page backPage={PAGE.REGISTER} navigation={navigation}>
      <View style={{ flex: 1 }}>
      {/* on android the carousel is not working */}
      <ErrorBoundary>
      <Carousel
        loop
        width={width}
        height={height-300}
        autoPlay={false}
        data={[...new Array(2).keys()]}
        scrollAnimationDuration={1000}
        onSnapToItem={(index) => !index ? dispatch(actionCreators.changeGenderMale()) :  dispatch(actionCreators.changeGenderFemale())}
        renderItem={({ index }) => (
          <View
            style={{
              flex: 1,
              borderWidth: 0,
              justifyContent: "center",
            }}
          >{index === 0 && (
                  <>
                     <Male3D />
                  </>
                )}
                {index === 1 && (
                  <>
                     <Female3D />
                  </>
                )}
                
          </View>
        )}
      />
      </ErrorBoundary>
      <View
        style={{
          height: height - 150,
        }}
      >
        <NextButton onPress={() => {
          navigation.navigate(PAGE.WIZARDDOGS);
        }} />
      </View>
    </View>
      
    </Page>
  );
};
