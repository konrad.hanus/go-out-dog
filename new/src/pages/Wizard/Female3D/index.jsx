import React, { Suspense } from "react";
import Setup from "../../../component/Molecules/Setup";
import Player from "../../../component/Atoms/PlayerWoman";
import Loading from "../../WizardYourDog/Loading";
import { Vector3 } from "three";
import { Text } from 'react-native';

const Female3D = () => (
  <Setup cameraFov={50} cameraPosition={new Vector3(0, 0.7, 0.7)} far={1000}>
    <Suspense fallback={Loading}>
      <Player
        key={"Player-3D"}
        position={[0, -0.1, 0]}
        rotation={[0, 0.7, 0]}
        scale={[0.4, 0.4, 0.4]}
        animation={3}
        isRotation={false}
      />
    </Suspense>
  </Setup>
);

export default Female3D;
