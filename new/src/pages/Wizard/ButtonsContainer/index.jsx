import React from "react";
import { View, StyleSheet } from "react-native";

const ButtonsContainer = ({ children }) => (
  <View style={styles.box}>
    <View style={styles.box}>{children}</View>
  </View>
);

export default ButtonsContainer;

const styles = StyleSheet.create({
  box: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
});
