import React, { Suspense } from "react";
import Setup from "../../../component/Molecules/Setup";
import Player from "../../../component/Atoms/PlayerMan";
import Loading from "../../WizardYourDog/Loading";
import { Vector3 } from "three";

const Male3D = () => (
  <Setup cameraFov={50} cameraPosition={new Vector3(0, 0.8, 0.7)} far={1000}>
    <Suspense fallback={Loading}>
      {/* <Player
        key={"Player-3D"}
        position={[0, 0, 0]}
        rotation={[0, 0.7, 0]}
        scale={[0.5, 0.5, 0.5]}
        animation={4}
        isRotation={false}
      /> */}
      <mesh position-y={0.5} castShadow>
        <boxGeometry />
        <meshPhongMaterial />
      </mesh>
    </Suspense>
  </Setup>
);

export default Male3D;
