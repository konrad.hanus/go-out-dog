import React from "react";
import { TouchableHighlight } from "react-native";
import GenderImage from "../../../component/Atoms/GenderImg";

const GenderContainer = ({ source, onPress }) => (
  <TouchableHighlight onPress={onPress}>
    <GenderImage source={source} />
  </TouchableHighlight>
);

export default GenderContainer;
