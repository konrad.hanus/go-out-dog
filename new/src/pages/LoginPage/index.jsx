import { Image } from 'react-native';
import { Box, Text, Heading, VStack, FormControl, Input, Link, Button, HStack, Center, NativeBaseProvider } from "native-base";
import psiaApkaLogo from './../../assets/png/logo512x512.png';
import { SafeAreaView } from 'react-native-safe-area-context';
import Page from './../Page';

const LoginPage = ({ route, navigation }) => {
    return (<Page isHome={true}><Center w="100%" h="100%" style={{justifyContent: 'center'}}>
                <Image 
                    source={psiaApkaLogo} 
                    style={{
                        widht: 200, 
                        height: 200, 
                        borderWidth: 0,
                        borderColor: 'red',
                        resizeMode: "contain",
                    }}
                    />    
                    <Box safeArea p="2" py="0" w="90%" maxW="290">
                    <Button
                    size="lg"
                    style={{  
                    borderRadius: 30}}
                    onPress={() =>
                        navigation.push('Permissions')
                      }
                    >Graj
                    </Button>

                    <Button
                    size="lg"
                    style={{  
                    borderRadius: 30}}
                    onPress={() =>
                        navigation.push('Fiber')
                      }
                    >Fiber
                    </Button>
                </Box>
            </Center></Page>);
}

export default LoginPage

