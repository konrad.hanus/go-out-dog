import { ImageBackground } from "react-native";
import { Center } from "native-base";
import Page from "../Page";
import Logo from "../../component/Atoms/LogoImage";
import HomePageButtons from "../../component/Molecules/HomePageButtons";
import { Asset, useAssets} from 'expo-asset';

const HomePage = ({ navigation }) => {

  const [assets, error] = useAssets([require('../../assets/png/render_compress.png')]);

  return (
    assets && <ImageBackground
      source={assets[0]}
      style={{ width: "100%", height: "100%" }}
    >
      <Page isHome={true}>
        <Center w="100%" h="100%" style={{ justifyContent: 'space-around' }}>
            <Logo width={259} height={259}/>
            <HomePageButtons navigation={navigation} />
        </Center>
      </Page>
    </ImageBackground>
  );
};

export default HomePage;
