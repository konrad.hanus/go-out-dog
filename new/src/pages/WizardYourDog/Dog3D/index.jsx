import React, { Suspense } from "react";
import { View, Dimensions } from "react-native";
import Setup from "../../../component/Molecules/Setup";

import Szarik from "../../../component/Atoms/Szarik";
import Chihuahua from "../../../component/Atoms/Chihuahua";
import Pinczer from "../../../component/Atoms/Pinczer";
import GreenDog from "../../../component/Atoms/GreenDog";

import Loading from "../Loading";
import { Vector3 } from "three";
import { DOGTYPE } from "../../WizardDogs/DogsData";
import { useSelector } from "react-redux";

const DogStrategy = ({dogType}) => {
  switch (dogType) {
    case DOGTYPE.Szarik:
      return (
        <Szarik
          key={"my-dog"}
          position={[0, -0.15, 0]}
          rotation={[0, 0.7, 0]}
          scale={[0.7, 0.7, 0.7]}
          animation={1}
          isRotation={false}
        />
      );

    case DOGTYPE.Pinczer:
      return (
        <Pinczer
          key={"my-dog"}
          position={[0, -0.15, 0]}
          rotation={[0, 0.7, 0]}
          scale={[0.7, 0.7, 0.7]}
          animation={1}
          isRotation={false}
        />
      );

    case DOGTYPE.Chihuahua:
      return (
        <Chihuahua
          key={"my-dog"}
          position={[0, -0.15, 0]}
          rotation={[0, 0.7, 0]}
          scale={[0.7, 0.7, 0.7]}
          animation={1}
          isRotation={false}
        />
      );

    case DOGTYPE.GreenDog:
      return (
        <GreenDog
          key={"my-dog"}
          position={[0, -0.15, 0]}
          rotation={[0, 0.7, 0]}
          scale={[0.7, 0.7, 0.7]}
          animation={1}
          isRotation={false}
        />
      );
  }
};
const Dog3D = () => {
  const dogType = useSelector((state) => state.registration.dogType);

  const height = Dimensions.get("window").height;

  return (
    <View
      style={{ width: 200, height: 200, alignSelf: "center", marginBottom: 30 }}
    >
      <Setup
        cameraFov={50}
        cameraPosition={new Vector3(0, 0.4, 0.8)}
        far={1000}
      >
        <Suspense fallback={Loading}>
          <DogStrategy dogType={dogType} />
        </Suspense>
      </Setup>
    </View>
  );
};

export default Dog3D;
