import Page from "../Page";
import { View, StyleSheet } from "react-native";
import React from "react";
import PAGE from "./../../pages/pages";
import Gender from "./Gender";
import DogInputs from "./DogInputs";
import DogsName from "./DogsName";
import Dog3D from "./Dog3D";
import PlayButton from "./PlayButton";
import male from "../Wizard/assets/male.png";
import female from "../Wizard/assets/female.png";
import { useSelector } from "react-redux";

export default WizardYourDogPage = ({ navigation }) => {
  const dogName = useSelector((state) => state.registration.dogName);
  const dogAge = useSelector((state) => state.registration.dogAge);
  const dogWeight = useSelector((state) => state.registration.dogWeight);
  const dogGender = useSelector((state)=> state.registration.dogGender);
  const dogType = useSelector((state) => state.registration.dogType);

  const dogLastInjection = useSelector(
    (state) => state.registration.dogLastInjection
  );

  return (
    <Page backPage={PAGE.WIZARDDOGS} navigation={navigation}>
      <View style={styles.centerContainer}>
        <View style={styles.rowContainer}>
          <DogsName>{dogName}</DogsName>
          <Gender source={dogGender === "Pies" ? male : female}/>
        </View>
        <Dog3D dogType={dogType}/>
        <DogInputs
          dogAge={dogAge}
          dogWeight={dogWeight}
          dogLastInjection={dogLastInjection}
        />
        <PlayButton navigation={navigation} />
      </View>
    </Page>
  );
};

const styles = StyleSheet.create({
  centerContainer: {
    justifyContent: "center",
    alignContent: "center",
  },
  rowContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-start",
    alignContent: "center",
  },
});
