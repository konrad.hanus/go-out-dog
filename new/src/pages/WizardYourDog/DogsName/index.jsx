import { Text } from "react-native";

const DogsName = ({children}) => ( <Text
    style={{
      fontWeight: "400",
      lineHeight: 117.72,
      fontSize: 64,
      color: "gray",
      textTransform: "uppercase",
    }}
  >{children}</Text>)

  export default DogsName;
