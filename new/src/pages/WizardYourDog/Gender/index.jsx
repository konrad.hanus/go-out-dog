import GenderImage from "../../../component/Atoms/GenderImg";
const Gender = ({ source }) => <GenderImage source={source} />;
export default Gender;
