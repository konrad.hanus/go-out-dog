import { View, Dimensions } from "react-native";
import { Input } from "native-base";
import { useDispatch } from "react-redux";
import { actionCreators } from "../../../containers/Registration/action";

const DogInputs = (props) => {
  const width = Dimensions.get("window").width;

  const dispatch = useDispatch();

  const onDogAgeChange = (dogAge) => {
    dispatch(actionCreators.changeDogAge(dogAge));
  };

  const onDogWeightChange = (dogWeight) => {
    dispatch(actionCreators.changeDogWeight(dogWeight));
  };

  const onDogLastInjectionChange = (dogLastInjection) => {
    dispatch(actionCreators.changeDogLastInjection(dogLastInjection));
  };

  return (
    <View
      style={{
        marginTop: width - 450,
        marginBottom: 20,
        alignContent: "center",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Input
        variant="rounded"
        placeholder="Wiek"
        width="80%"
        height={20}
        borderRadius={30}
        marginTop={2}
        size={16}
        value={props.dogAge}
        onChangeText={onDogAgeChange}
      />
      <Input
        variant="rounded"
        placeholder="Waga"
        width="80%"
        height={20}
        borderRadius={30}
        marginTop={2}
        size={16}
        value={props.dogWeight}
        onChangeText={onDogWeightChange}
      />
      <Input
        variant="rounded"
        placeholder="Ostatnie Szczepienie"
        width="80%"
        height={20}
        borderRadius={30}
        marginTop={2}
        size={16}
        value={props.dogLastInjection}
        onChangeText={onDogLastInjectionChange}
      />
    </View>
  );
};

export default DogInputs;
