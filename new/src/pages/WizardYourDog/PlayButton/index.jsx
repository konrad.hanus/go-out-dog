import HomeButton from "../../../component/Atoms/HomeButton";
import { View } from "react-native";
import PAGE from "./../../../pages/pages";

const PlayButton = ({ navigation }) => (
  <View style={{ alignItems: "center", justifyContent: "center" }}>
    <HomeButton
      width="50%"
      onPress={() => navigation.navigate(PAGE.GAME)}
      title="Graj"
    />
  </View>
);

export default PlayButton;
