export default  {
    REGISTER: "Register",
    SEARCH: "Search",
    LOGIN: "Login",
    WALKING: "Walking",
    WIZARDDOGS: "WizardDogs",
    WIZARD: "Wizard",
    WIZARDYOURDOG: "WizardYourDog",
    GAME: "Game",
    HOME: "Home",
    WELCOME: "Welcome",
    SIGNIN: "SignIn",
    PERMISSION: "Permissions"
}