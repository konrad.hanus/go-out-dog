import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import { Input, Icon, Pressable } from "native-base";
import MaterialIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { useDispatch } from "react-redux";
import { actionCreators } from "../../../containers/Registration/action";

const SignInInputs = (props) => {
  const [show, setShow] = useState(false);

  const dispatch = useDispatch();

  const onNickChange = (nick) => {
    dispatch(actionCreators.changeNick(nick));
  };

  const onEmailChange = (email) => {
    dispatch(actionCreators.changeEmail(email));
  };

  const onPasswordChange = (password) => {
    dispatch(actionCreators.changePassword(password));
  };
  
  return (
    <View style={styles.inputsContainer}>
      {props.isNameInputExist && (
        <Input
          variant="rounded"
          placeholder="Nick"
          width="80%"
          height={20}
          borderRadius={30}
          marginTop={2}
          size={16}
          value={props.nick}
          onChangeText={onNickChange}
        />
      )}
      <Input
        variant="rounded"
        placeholder="Email"
        width="80%"
        height={20}
        borderRadius={30}
        marginTop={3}
        size={16}
        value={props.email}
        onChangeText={onEmailChange}
      />
      <Input
        variant="rounded"
        placeholder="Hasło"
        width="80%"
        height={20}
        borderRadius={30}
        margin={3}
        size={16}
        value={props.password}
        onChangeText={onPasswordChange}
        type={show ? "text" : "password"}
        InputRightElement={
          <Pressable onPress={() => setShow(!show)}>
            <Icon
              as={<MaterialIcons name={show ? "eye" : "eye-off"} />}
              size={7}
              mr="5"
              color="muted.400"
            />
          </Pressable>
        }
      />
    </View>
  );
};

export default SignInInputs;

const styles = StyleSheet.create({
  inputsContainer: {
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
  },
});
