import { View, Text } from "react-native";
import {Bar} from 'react-native-progress';

const LoadingProgressBar = ({nextI, sumItems}) => {
  
  const a = nextI/sumItems;
  return(<View
    style={{
      width: 400,
      height: 100,
      position: "absolute",
      top: 600,
      zIndex: 199,
      justifyContent: 'center', 
      alignItems: 'center'
    }}
  >{a < 1 && <><Text>{nextI}/{sumItems}</Text>
     <Bar progress={a} width={200} /></> }
  </View>);

}
export default LoadingProgressBar;