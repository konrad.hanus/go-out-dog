import * as React from "react";
import BoxButtons from "../../Atoms/HomeButtons";
import HomeButton from "../../Atoms/HomeButton";
import {View} from 'react-native';

const HomePageButtons = ({ navigation }) => (
  <BoxButtons maxW="325" style={{height: 300, marginTop: 50}}>

    <HomeButton
      title="Graj"
      width="100%"
      onPress={() => navigation.push("Permissions")}
    />
    <HomeButton
      title="Załóż Konto"
      width="100%"
      onPress={() => navigation.push("Welcome")}
    />
  </BoxButtons>
);

export default HomePageButtons;
