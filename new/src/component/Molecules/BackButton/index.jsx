import {View } from 'react-native';
import IconBack from '../../Atoms/IconBack';

const BackButton = ({navigation, page}) => (<View
    style={{
      flexDirection: "row",
      justifyContent: "space-around",
      alignItems: "flex-start",
      alignContent: "center",
      width: "20%",
    }}
  >
    <View
      style={{
        alignItems: "flex-start",
        alignContent: "center",
        margin: 20,
      }}
    >
      <IconBack onPress={() => navigation.navigate(page)} />
    </View>
  </View>);

  export default BackButton;