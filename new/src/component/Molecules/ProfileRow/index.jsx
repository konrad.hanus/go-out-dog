import { HStack} from "native-base";
import ProfileBox from '../../Atoms/ProfileBox';

const ProfileRow = ({first, second, third}) => <HStack space={3} justifyContent="center" margin="2">
{first && <ProfileBox color={first.backgroundColor} name={first.name} />}
{second && <ProfileBox color={second.backgroundColor} name={second.name} />}
{third && <ProfileBox color={third.backgroundColor} name={third.name} />}
</HStack>

export default ProfileRow;