
import { useEffect, useState } from 'react';
import ModelImport from '../../Atoms/ModelImport';
export default DogProfile = (props) => {

    const [heading, setHeading] = useState(0);

    useEffect(() => {
        // props.rotationValue && setHeading(props.rotationValue);

        // console.log('rx',heading)

    }, [props])
    //stacy_lightweight.glb
    //gooddrwalwalking.glb
    //filipdrwaldancing.glb
    //drwalwalking.glb
    //goodColoredWithAnimation.glb

    //http://srv48006.seohost.com.pl/1/pinczermove.glb
    //http://srv48006.seohost.com.pl/1/szarikmove.glb
    //http://srv48006.seohost.com.pl/1/chihuahua.glb
    //http://srv48006.seohost.com.pl/1/dog.glb
    return <ModelImport
        name="DogProfile"
        url="http://srv48006.seohost.com.pl/1/dog.glb"
        player={false}
        power={0.7}
        setIsLoading={props.setIsLoading}
        isDogAvatar={false}
        animation={1}
        isRotate={false}
        isDog={true}
        isDogProfile={true}
        incrementCounterLoaded={props.incrementCounterLoaded} />
}