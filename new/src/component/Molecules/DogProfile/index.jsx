import React, { Suspense } from 'react'
import { Canvas } from '@react-three/fiber/native'
import DOG_TYPES from '../../../types/DOG_TYPES';
import Dog from '../Dog/Dog';

 const Loading = () => <Text>ładowanie</Text>

export default function DogProfile() {
  return (
    <Canvas frameloop={'always'}>
      <Suspense fallback={Loading}>
      <ambientLight intensity={0.1} />
      <directionalLight color="red" position={[0, 0, 5]} />
      <ambientLight intensity={0.5} />
        <Dog 
            key={'DogProfile'}
            position={[0,-0.3,4.2]} 
            rotation={[0,0.55,0]} 
            animation={1} 
            dogType={DOG_TYPES.szarik}
            isRotation={false}/>
      </Suspense>
    </Canvas>
  )
}