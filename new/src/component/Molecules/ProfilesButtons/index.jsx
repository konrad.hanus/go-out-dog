
import { StyleSheet } from "react-native";
import { View } from "native-base";
import ModalButton from "../../Atoms/ModalButton";

const ProfilesButtons = ({
    onPressLeft, 
    onPressRight, 
    titleLeft, 
    titleRight, 
    colorLeft, 
    colorRight,}) => <View style={styles.buttonsContainer}>
    <ModalButton isleftButton={true} title={titleLeft} onPress={onPressLeft} color={colorLeft} />
    <ModalButton isleftButton={false} title={titleRight} onPress={onPressRight} color={colorRight}/>
</View>;

export default ProfilesButtons;

const styles = StyleSheet.create({
    buttonsContainer: {
        paddingTop: 10,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "flex-start",
        alignContent: "flex-start",
        width: "100%",
    },
});