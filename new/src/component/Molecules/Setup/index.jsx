import React from 'react';
import { Canvas, useThree } from '@react-three/fiber/native';
import { Vector3 } from 'three';
import { PerspectiveCamera } from '@react-three/drei/core';

const deg2rad = degrees => degrees * (Math.PI / 180);

const Cameraa = ({cameraPosition, far, cameraFov}) => {
  
  useThree(({camera}) => {
    camera.rotation.set(deg2rad(-35), 0, 0);
  });
  
  return <PerspectiveCamera 
  makeDefault 
  fov={cameraFov}
  position={cameraPosition}
  far={far} />
}

export const Setup = ({
    children,
    cameraFov = 60,
    cameraPosition = new Vector3(0, 3.5, 6),
    far = 1000,
    controls = true,
    lights = true,
    ...restProps
  }) =>  {
   
    return(<Canvas 
    shadows 
    camera={{ 
    position: cameraPosition, 
    cameraFov, 
    far,
     }} {...restProps}>
      {children}
      {lights && (
        <>
        <Cameraa cameraFov={cameraFov} cameraPosition={cameraPosition} far={far}/>
          <ambientLight intensity={0.3} />
          <pointLight intensity={0.3} position={[0, 6, 0]} />
          <pointLight intensity={0.3} position={[-5,0, 5]} />
          <pointLight intensity={0.3} position={[-5, 6, -5]} />
          <pointLight intensity={0.3} position={[5, 6, 5]} />
          <pointLight intensity={0.3} position={[5, 6, 5]} />
        </>
      )}
    </Canvas>
  );
      }

export default Setup;