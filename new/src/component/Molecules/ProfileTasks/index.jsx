
import { StyleSheet } from "react-native";
import { View} from "native-base";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Foundation from 'react-native-vector-icons/Foundation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import TaskBox from "../../Organisms/TaskBox";

const ProfileTasks = () => <View style={styles.tasksContainer}>
              <TaskBox
                icon={<Foundation name="guide-dog" size={29} />}
                titleTextOne="Idź na spacer"
                titleTextTwo="0,00 km / 2,00 km"
                isSingleTitle={false}
                isGreenBg={true}
              />
              <TaskBox
                icon={<Ionicons name="tennisball" size={20} />}
                titleTextOne="Bawcie się"
                isSingleTitle={true}
                isGreenBg={false}
              />
              <TaskBox
                icon={<MaterialCommunityIcons name="candy" size={29} />}
                titleTextOne="Daj smakołyka"
                isSingleTitle={true}
                isGreenBg={true}
              />
              <TaskBox
                icon={<Entypo name="camera" size={22} />}
                titleTextOne="Zrób super zdjęcie"
                isSingleTitle={true}
                isGreenBg={false}
              />
              <TaskBox
                icon={<MaterialCommunityIcons name="treasure-chest" size={25} />}
                titleTextOne="Odkopcie razem skarb"
                isSingleTitle={true}
                isGreenBg={true}
                isSpace={false}
              />
            </View>;

export default ProfileTasks;

const styles = StyleSheet.create({
    tasksContainer: {
      marginTop: 30,
      flexDirection: "column",
      justifyContent: "space-around",
      alignItems: "center",
      alignContent: "center",
      width: "100%",
    },
  });
  