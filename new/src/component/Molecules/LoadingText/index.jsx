import { View, Text } from "react-native";

const LoadingText = () => (<View
    style={{
      width: 400,
      height: 100,
      position: "absolute",
      top: 700,
      zIndex: 199,
      justifyContent: 'center', 
      alignItems: 'center'
      
    }}
  >
    <Text style={{ fontSize: 30}}>Rozglądaj się gdy grasz i </Text>
    <Text style={{ fontSize: 25, paddingLeft: 20 }}>baw się dobrze!</Text>
  </View>);
                

export default LoadingText;

