import React from "react";
import { View, StyleSheet } from "react-native";
import Logo from "../../Atoms/LogoImage";
import IconBack from "../../Atoms/IconBack";

const NavLogo = ({ navigation }) => (
  <View style={styles.iconWithLogoContainer}>
    <View style={styles.iconContainer}>
      <IconBack onPress={() => navigation.push("Welcome")} />
    </View>
    <View>
      <Logo width={142} height={142} />
    </View>
  </View>
);

export default NavLogo;

const styles = StyleSheet.create({
  iconWithLogoContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "flex-start",
    alignContent: "center",
    width: "80%",
  },
  iconContainer: {
    alignItems: "flex-start",
    alignContent: "center",
  },
});
