import { View, Image } from "react-native";
import dogs from './dogs.png';

const LoadingScreen = () => (<View
    style={{
        width: 414,
        height: 946,
        position: "absolute",
        left: 0,
        top: -40,
        zIndex: 199,
    }}
  >
    <Image
      style={{
       
        width: 414, 
        height: 946
      }}
      source={dogs}
    ></Image>
  </View>);
                

export default LoadingScreen;

