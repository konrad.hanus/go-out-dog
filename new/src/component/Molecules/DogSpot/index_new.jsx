import React, { Suspense } from 'react';
import {View} from 'react-native';
import { Canvas } from '@react-three/fiber/native'
import DOG_TYPES from '../../../types/DOG_TYPES';
import { Vector3 } from 'three'
import Player from '../../Atoms/PlayerMan';
import DogSpot from '../../Atoms/DogSpot'
 const Loading = () => <Text>ładowanie</Text>

export default function DogMap(props) {

  const cameraFov = 20;
  const far = 1000;
  const intensity = 200;
  const cameraPosition = new Vector3(0, 6, -4);
  return (
    <View 
     style={{borderWidth: 0, borderColor: 'pink', width: 300, height: 300, top:100}}>
    <Canvas frameloop={'always'} camera={{ 
        position: cameraPosition, 
        fov: cameraFov, 
        far }}>
      <Suspense fallback={Loading}>
      <directionalLight color="white" position={[5, 1, -5]} intensity={intensity} />
      <directionalLight color="white" position={[5, 1, 5]} intensity={intensity} />
      <directionalLight color="white" position={[-5, 1, -5]} intensity={intensity} />
      <directionalLight color="white" position={[-5, 1, -5]} intensity={intensity} />
        <DogSpot 
            position={[0.0,-1.0,0]} 
            rotation={[0,0,0]} 
            isRotation={true}
            />
      </Suspense>
    </Canvas>
    </View>
  )
}