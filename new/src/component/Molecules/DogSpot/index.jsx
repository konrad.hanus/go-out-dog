import ModelImport from '../../Atoms/ModelImport';

export default DogSpot = (props) => <ModelImport
    name="DogSpot"
    url="http://srv48006.seohost.com.pl/1/dogSpot.glb"
    power={400}
    rotate={true}
    setIsLoading={props.setIsLoading} 
    incrementCounterLoaded={props.incrementCounterLoaded}
/>