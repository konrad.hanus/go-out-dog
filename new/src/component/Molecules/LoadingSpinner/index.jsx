import { View, ActivityIndicator } from "react-native";

const LoadingScreen = () => (<View
    style={{
      width: 100,
      height: 100,
      position: "absolute",
      left: 20,
      top: 90,
      zIndex: 199,
    }}
  >
    <ActivityIndicator size="large" color="#000000" style={{ zIndex: 100 }} />
  </View>);
                

export default LoadingScreen;

