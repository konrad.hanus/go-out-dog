
import { useEffect, useState } from 'react';
import ModelImport from '../../Atoms/ModelImport';
export default Player = (props) => {

    const [heading, setHeading] = useState(0);

    useEffect(() => {
        // props.rotationValue && setHeading(props.rotationValue);

        // console.log('rx',heading)

    }, [props])
    //stacy_lightweight.glb
    //gooddrwalwalking.glb
    //filipdrwaldancing.glb
    //drwalwalking.glb
    //goodColoredWithAnimation.glb
    return <ModelImport
        name="DogAvatar"
        url="http://srv48006.seohost.com.pl/1/szarikmove.glb"
        player={true}
        power={0.7}
        setIsLoading={props.setIsLoading}
        isDogAvatar={true}
        animation={1}
        isRotate={false}
        isDog={true}
        incrementCounterLoaded={props.incrementCounterLoaded} />
}