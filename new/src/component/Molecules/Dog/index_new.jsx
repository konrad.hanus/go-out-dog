import React, { Suspense } from "react";
import { View } from "react-native";

import DogPlayer from "./DogPlayer";
import MapRotation from "../../Atoms/MapRotation";
import Setup from "./../Setup";
import DogSpot from "../../Atoms/DogSpot";
import DogGym from "../../Atoms/DogGym";
import DogGym1 from "../../Atoms/DogGym/index1";
import DogGym2 from "../../Atoms/DogGym/index2";
import DogGym3 from "../../Atoms/DogGym/index3";
import DogGym4 from "../../Atoms/DogGym/index4";
import DogGym5 from "../../Atoms/DogGym/index5";
import DogGym6 from "../../Atoms/DogGym/index6";
import DogGym7 from "../../Atoms/DogGym/index7";
import DogGym8 from "../../Atoms/DogGym/index8";
import DogGym9 from "../../Atoms/DogGym/index9";


const DOG_GYMS = {
  0: DogGym,
  1: DogGym1,
  2: DogGym2,
  3: DogGym3,
  4: DogGym4,
  5: DogGym5,
  6: DogGym6,
  7: DogGym7,
  8: DogGym8,
  9: DogGym9,
}
const Loading = () => <Text>ładowanie</Text>;

export default function DogMap(props) {
  const dogGymArray =
    props.dogGyms &&
    Object.keys(props.dogGyms).map((key) => props.dogGyms[key]);


  const dogsGyms = dogGymArray
    ? dogGymArray.map((dogGym) => ({
        lat: dogGym.location.latitude,
        long: dogGym.location.longitude,
      }))
    : [];

  const dogGymArrayConverted =
    typeof dogsGyms !== "undefined" && dogsGyms.length > 0
      ? dogsGyms.map((dogGymItem) => ({
          lat: dogGymItem.lat,
          long: dogGymItem.long,
        }))
      : {
          lat: 0,
          long: 0,
        };

  const myHouse = {
    lat: props.userLocation.latitude,
    long: props.userLocation.longitude,
  };
  log("DLUGOSC!!!!", dogGymArrayConverted);
  return (
    // <View
    //  style={{borderWidth: 1, borderColor: 'pink', width: 200, height: 200, top:100}}>
    <Setup>
      <Suspense fallback={Loading}>
        <MapRotation>
          <DogPlayer />
          {/* <DogSpot  position={[0.5,0,0]}  />  */}

          {typeof dogGymArrayConverted !== "undefined" &&
            dogGymArrayConverted.length > 0 &&
            dogGymArrayConverted.map((item, key) =>
              key === 0 ? (
                <DogGym
                  indetyfier={key}
                  key={`doggym-${key}`}
                  position={[
                    myHouse.long - item.long,
                    -0.8,
                    myHouse.lat - item.lat,
                  ]}
                />
              ) : key === 1 ? (
                <DogGym1
                  indetyfier={key}
                  key={`doggym-${key}`}
                  position={[
                    myHouse.long - item.long,
                    -0.8,
                    myHouse.lat - item.lat,
                  ]}
                />
              ) : key === 2 ? (
                <DogGym2
                  indetyfier={key}
                  key={`doggym-${key}`}
                  position={[
                    myHouse.long - item.long,
                    -0.8,
                    myHouse.lat - item.lat,
                  ]}
                />
              ): key === 3 ? (
                <DogGym3
                  indetyfier={key}
                  key={`doggym-${key}`}
                  position={[
                    myHouse.long - item.long,
                    -0.8,
                    myHouse.lat - item.lat,
                  ]}
                />
              ) : key === 4 ? (
                <DogGym4
                  indetyfier={key}
                  key={`doggym-${key}`}
                  position={[
                    myHouse.long - item.long,
                    -0.8,
                    myHouse.lat - item.lat,
                  ]}
                />
              ) : key === 5 ? (
                <DogGym5
                  indetyfier={key}
                  key={`doggym-${key}`}
                  position={[
                    myHouse.long - item.long,
                    -0.8,
                    myHouse.lat - item.lat,
                  ]}
                />
              ) : key === 6 ? (
                <DogGym6
                  indetyfier={key}
                  key={`doggym-${key}`}
                  position={[
                    myHouse.long - item.long,
                    -0.8,
                    myHouse.lat - item.lat,
                  ]}
                />
              ) : key === 7 ? (
                <DogGym7
                  indetyfier={key}
                  key={`doggym-${key}`}
                  position={[
                    myHouse.long - item.long,
                    -0.8,
                    myHouse.lat - item.lat,
                  ]}
                />
              ) : key === 8 ? (
                <DogGym8
                  indetyfier={key}
                  key={`doggym-${key}`}
                  position={[
                    myHouse.long - item.long,
                    -0.8,
                    myHouse.lat - item.lat,
                  ]}
                />
              ) : (
                <DogGym9
                  indetyfier={key}
                  key={`dogspot-${key}`}
                  position={[
                    myHouse.long - item.long,
                    -0.8,
                    myHouse.lat - item.lat,
                  ]}
                />
              )
            )}
          {/* <DogGym  position={[dogGym.lag,dogGym.lat,0]}  />  */}
        </MapRotation>
      </Suspense>
    </Setup>
    // </View>
  );
}
