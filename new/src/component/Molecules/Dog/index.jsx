
import { useEffect, useState } from 'react';
import ModelImport from '../../Atoms/ModelImport';
export default Player = (props) => {

    const [heading, setHeading] = useState(0);
    
    useEffect(() => {
        // props.rotationValue && setHeading(props.rotationValue);

        // console.log('rx',heading)

    }, [props])
    //stacy_lightweight.glb
    //gooddrwalwalking.glb
    //filipdrwaldancing.glb
    //drwalwalking.glb
    return <ModelImport
        name="Dog"
        url="http://srv48006.seohost.com.pl/1/dog.glb"
        player={true}
        power={0.7}
        setIsLoading={props.setIsLoading}
        animation={1}
        isRotate={true}
        isDog={true}
        incrementCounterLoaded={props.incrementCounterLoaded} />
}