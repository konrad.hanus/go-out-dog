import React, { useRef } from 'react';
import DOG_TYPES from '../../../types/DOG_TYPES';
import Dog from './Dog';
import Player from '../../Atoms/PlayerMan';


const DogPlayer = () => {

    return( 
    <>
        <Dog 
            position={[0.28,-0.8,0]} 
            // position={[0,-0.8,0]}
            rotation={[0,0,0]} 
            animation={1} 
            dogType={DOG_TYPES.szarik}
            isRotation={false}
            key={"DogMap"}/>
        <Player 
            position={[-0.25,-0.8,0]} 
            rotation={[0,0,0]} 
            animation={5} 
            dogType={DOG_TYPES.szarik}
            isRotation={false}/>
    </>)
}

export default DogPlayer;