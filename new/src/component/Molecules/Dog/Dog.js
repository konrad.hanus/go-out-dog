import DOG_TYPES from "../../../types/DOG_TYPES";
import Chihuahua from '../../Atoms/Chihuahua';
import Pinczer from '../../Atoms/Pinczer';
import Szarik from '../../Atoms/Szarik';
import GreenDog from '../../Atoms/GreenDog';

const Dog = (props) => {
    const { dogType } = props;

    switch(dogType){
        case DOG_TYPES.chihuahua:
            return <Chihuahua {...props} />;
        case DOG_TYPES.pinczer:
            return <Pinczer {...props} />;
        case DOG_TYPES.szarik:
            return <Szarik {...props} />;
        case DOG_TYPES.dog:
            return <GreenDog {...props}/>;
    }
  }

  export default Dog;