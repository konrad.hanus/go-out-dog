import { Text } from "native-base";
import ProfileTextName from '../../Atoms/ProfileTextName';
import ExpBar from '../../Atoms/ExpBar';

export default ProfielExpBar = ({name, level}) => <>
    <ProfileTextName name={name} />
    <Text>{`(level ${level})`}</Text>
    <ExpBar />
    <Text>Doświadczenie</Text>
    </>