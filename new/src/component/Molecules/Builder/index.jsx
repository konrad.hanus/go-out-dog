import AddDogGym from "../../Atoms/AddDogGym";
import AddDogSpot from "../../Atoms/AddDogSpot";
import AddTreasure from "../../Atoms/AddTreasure";

const Builder = ({setMyLocation, fetchDogsGymLocation, fetchDogsSpotLocation, fetchTreasuresLocation, userLocation}) => {
    return (
        <>
          <AddDogGym
            onPress={() => {
              setMyLocation(userLocation, "dogGym");
              fetchDogsGymLocation();
              alert("Dodano tutaj dog gym'a");
            }}
          />
          <AddDogSpot
            onPress={() => {
              setMyLocation(userLocation, "dogSpot");
              fetchDogsSpotLocation();
              alert("Dodano tutaj dogSpot'a");
            }}
          />
          <AddTreasure
            onPress={() => {
              setMyLocation(userLocation, "treasure");
              fetchTreasuresLocation();
              alert("Zakopano tutaj skarb");
            }}
          />
        </>
      );
}

export default Builder;