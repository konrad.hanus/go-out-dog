import React, { Suspense } from 'react';
import {View} from 'react-native';
import { Canvas } from '@react-three/fiber/native'
import { Vector3 } from 'three'
import Player from '../../Atoms/PlayerMan';
 const Loading = () => <Text>ładowanie</Text>

export default function PlayerAvatar(props) {

  const cameraFov = 20;
  const far = 1000;
  const cameraPosition = new Vector3(0, 0, 5);
  return (
    <View 
     style={{borderWidth: 0, borderColor: 'pink', width: 200, height: 200, top:20}}>
    <Canvas frameloop={'always'} camera={{ 
        position: cameraPosition, 
        fov: cameraFov, 
        far }}>
      <Suspense fallback={Loading}>
      <ambientLight intensity={0.1} />
      <directionalLight color="red" position={[0, 0, 5]} />
      <ambientLight intensity={0.5} />
        <Player 
            position={[-0.63,-0.4,0]} 
            rotation={[0,0,0]} 
            animation={5} 
            isRotation={true}/>
      </Suspense>
    </Canvas>
    </View>
  )
}