import * as React from "react";
import { StyleSheet } from "react-native";
import BoxButtons from "../../Atoms/HomeButtons";
import WelcomeButton from "../../Atoms/WelcomeButton";

const WelcomePageButtons = ({ navigation }) => (
  <BoxButtons maxW="147" style={styles.box}>
    <WelcomeButton
      titleStyle={true}
      bgColor="#32CD32"
      title="Rejestracja"
      marginRight={4}
      onPress={() => navigation.push("Register")}
    />
    <WelcomeButton
      titleStyle={false}
      bgColor="white"
      title="Logowanie"
      marginLeft={4}
      onPress={() => navigation.push("SignIn")}
    />
  </BoxButtons>
);

export default WelcomePageButtons;

const styles = StyleSheet.create({
  box: {
    justifyContent: "center",
    flexDirection: "row",
  },
});
