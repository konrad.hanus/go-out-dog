import {useState} from 'react';
import { Dimensions, StyleSheet } from "react-native";
import DogProfile from "../../Molecules/DogProfile";
import { View, Center, Modal, Button, Text, HStack} from "native-base";
import ProfileExpBar from "../../Molecules/ProfileExpBar";

import ProfileTasks from "../../Molecules/ProfileTasks";
import ProfileDivider from "../../Atoms/ProfileDivider";
import ProfilesButtons from "../../Molecules/ProfilesButtons";
import { SUBPAGES } from "../../Pages/ModalProfile/types";
import ProfileTiles from '../../Organisms/ProfileTiles';

const ProfileTemplate = ({ showModal, setShowModal, incrementCounterLoaded, isLoading, setIsLoading }) => {

  const [subPage, setSubPage] = useState(SUBPAGES.TASKS);
  const [nameButtonLeft, setNameButtonLeft] = useState("NAUKA");
  const [nameButtonRight, setNameButtonRight] = useState("ZABAWA");
  
  const [colorRight, setColorRight] = useState("#32C11E");
  const [colorLeft, setColorLeft] = useState("#0066FF");

  const greenButtonColor = "#32C11E";
  const redButtonColor = "#FF005C";
  const blueButtonColor = "#0066FF";

  const onPressLeft = () => {

    if(SUBPAGES.TASKS === subPage)
    {
      setSubPage(SUBPAGES.LEARN)

      setNameButtonLeft('ZADANIA');
      setColorLeft(redButtonColor);

      setNameButtonRight('ZABAWA');
      setColorRight(greenButtonColor);
    }

    if(SUBPAGES.LEARN === subPage)
    {
      setSubPage(SUBPAGES.TASKS)
      
      setNameButtonLeft('NAUKA');
      setColorLeft(blueButtonColor);

      setNameButtonRight('ZABAWA');
      setColorRight(greenButtonColor);
    }


    if(SUBPAGES.FUN === subPage)
    {
      setSubPage(SUBPAGES.LEARN)
      
      setNameButtonLeft('ZADANIA');
      setColorLeft(redButtonColor);

      setNameButtonRight('ZABAWA');
      setColorRight(greenButtonColor);
    }
  }
  

  const onPressRight = () => {

    if(SUBPAGES.TASKS === subPage)
    {
      setSubPage(SUBPAGES.FUN)

     setNameButtonLeft('NAUKA');
     setColorLeft(blueButtonColor);

     setNameButtonRight('ZADANIA');
     setColorRight(redButtonColor);
    }

    if(SUBPAGES.FUN === subPage)
    {

     setSubPage(SUBPAGES.TASKS)

     setNameButtonLeft('NAUKA');
     setColorLeft(blueButtonColor);

     setNameButtonRight('ZABAWA');
     setColorRight(greenButtonColor);
    }

    if(SUBPAGES.LEARN === subPage)
    {

     setSubPage(SUBPAGES.FUN)

     setNameButtonLeft('NAUKA');
     setColorLeft("#0066FF");

     setNameButtonRight('ZADANIA');
     setColorRight("#FF005C");
    }

   
  }

  const tilesFun = [
    {
        name: "Szukanie smakołyków", 
        backgroundColor: "yellow.300"
    }, 
    {
        name: "Tunel i skoki", 
        backgroundColor: "yellow.600"
    }, 
    {
        name: "Zabawa w chowanego", 
        backgroundColor: "yellow.700"
    }, {
        name: "Przeciąganie szunrkiem", 
        backgroundColor: "blue.300"
    }, 
    {
        name: "Nauka imion zabawek", 
        backgroundColor: "blue.500"
    }, {
        name: "Sprządanie zabawek", 
        backgroundColor: "blue.700"
    }, {
        name: "Aport", 
        backgroundColor: "green.300"
    }
    , {
        name: "Gonitwa za psem", 
        backgroundColor: "green.500"
    }
    ,{
        name: "Nowe sztuczki i powtarzanie komend", 
        backgroundColor: "green.700"
    }];


    const tilesKnowladge = [
      {
          name: "Siad", 
          backgroundColor: "primary.300"
      }, 
      {
          name: "Zostaw", 
          backgroundColor: "primary.300"
      }, 
      {
          name: "Do mnie", 
          backgroundColor: "primary.300"
      }, {
          name: "Waruj", 
          backgroundColor: "secondary.300"
      }, 
      {
          name: "Leżć", 
          backgroundColor: "secondary.500"
      }, {
          name: "Daj głos", 
          backgroundColor: "secondary.700"
      }, {
          name: "Na miejsce", 
          backgroundColor: "red.300"
      }
      , {
          name: "Aport", 
          backgroundColor: "red.500"
      }
      ,{
          name: "Zostań", 
          backgroundColor: "red.700"
      }];
      
    
  return (
    <Modal
      style={{ zIndex: 99 }}
      size="full"
      isOpen={showModal}
      onClose={() => setShowModal(false)}
    >
      <View style={styles.dogContainer}>
        {showModal && (
          <DogProfile
            setIsLoading={setIsLoading}
            incrementCounterLoaded={incrementCounterLoaded}
          />
        )}
        <View></View>
      </View>
      <Modal.Content style={styles.modalContent}>
        <Modal.Body style={styles.modalBody}>
          <View style={styles.mainContainer}>
            <ProfileExpBar name={"Figa"} level="10"/>
            <ProfilesButtons 
                onPressLeft={onPressLeft} 
                onPressRight={onPressRight}
                titleLeft={nameButtonLeft}
                titleRight={nameButtonRight}
                colorLeft={colorLeft}
                colorRight={colorRight}/>
            
            {subPage === SUBPAGES.FUN ? 
           <ProfileTiles title="zabawy" tiles={tilesFun} />
           : subPage === SUBPAGES.TASKS ? 
            <>
            <ProfileDivider>Dzisiejsze zadania</ProfileDivider>
            <ProfileTasks />
            </> : subPage === SUBPAGES.LEARN &&
            <ProfileTiles title="Nauka sztuczek" tiles={tilesKnowladge} />
            }
          </View>
          <Center flex={1} px="3">
            <Button.Group space={2}>
              <Button
                variant="ghost"
                colorScheme="blueGray"
                onPress={() => {
                  setShowModal(false);
                }}
              >
                Cancel
              </Button>
            </Button.Group>
          </Center>
        </Modal.Body>
      </Modal.Content>
    </Modal>
  );
};

export default ProfileTemplate;

const windowHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  dogContainer: {
    width: 200,
    height: 200,
    position: "absolute",
    top: 50,
    zIndex: 100,
  },
  modalContent: {
    marginBottom: 0,
    marginTop: "auto",
    zIndex: 99,
  },
  modalBody: {
    zIndex: 99,
    backgroundColor: "white",
  },
  mainContainer: {
    zIndex: 100,
    height: windowHeight - 250,
    margin: 0,
    flex: 1,
    paddingTop: 60,
    alignItems: "center",
    alignContent: "center",
  },
  buttonsContainer: {
    paddingTop: 10,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "flex-start",
    alignContent: "flex-start",
    width: "100%",
  },
  taskTitleContainer: {
    width: "100%",
    paddingTop: 25,
    paddingBottom: 0,
  },
  divider: {
    height: 1,
    backgroundColor: "#9FAEB0",
  },
  todayTasksText: {
    textAlign: "center",
    alignSelf: "center",
    width: 170,
    marginTop: -10,
    height: 20,
    color: "gray",
    fontSize: 11,
    letterSpacing: 1.25,
    backgroundColor: "white",
    textTransform: "uppercase",
    fontWeight: "bold",
  },
  tasksContainer: {
    marginTop: 30,
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    alignContent: "center",
    width: "100%",
  },
  iconWithContentContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    alignContent: "center",
    width: "100%",
  },
});
