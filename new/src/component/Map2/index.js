import MapView, { Circle } from "react-native-maps";
import { View, Text, Button, Dimensions } from "react-native";
import { useState, useRef, useEffect } from "react";
import mapStyle from "../mapStyle";
import SCALE from "./../../helpers/scale";
const Map2 = (props) => {

  const [userLocation, setUserLocation] = useState({ latitude:0, longitude:0 });

  useEffect(() => {
    props.handleZoom(undefined, -65, SCALE, 2000);
  });

  const setCurrentUserLocation = async ({ latitude, longitude }) => {
    // console.log("click");
    const camera = await props.mapRef.current.getCamera();
    // alert("zmiana");
    // console.log('camera', camera);
    props.mapRef.current.animateCamera(
      {
        ...camera,
        center: {
          latitude: latitude,
          longitude: longitude,
        },
      },
      { duration: 20 }
    );

    // log('aaa', latitude, longitude)

    // console.log("", props.mapRef.current.center);

    // props.mapRef.current.animateToRegion({
    //   latitude: latitude,
    //   longitude: longitude,
    //   latitudeDelta: 0.00022,
    //   longitudeDelta: 0.0021,
    // });
  };

  const setCurrent = () => {
    // setCurrentUserLocation(userLocation.latitude, userLocation.longitude);
  };
  const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } =
    Dimensions.get("window");
    
  return (
    <View style={{borderWidth: 5, 
      borderColor: 'black'}}>
      <MapView
        ref={props.mapRef}
        style={{
          height: SCREEN_HEIGHT,
          width: SCREEN_WIDTH,
          // zIndex: -9991,
          borderWidth: 2, 
          borderColor: 'grey'
        }}
         initialCamera={props.sharedCamera}
        // initialRegion={{
        //   accuracy: 35,
        //   altitude: 17.169769287109375,
        //   altitudeAccuracy: 5.861889362335205,
        //   heading: -1,
        //   latitude: 51.086781129189376,
        //   longitude: 17.05304514781193,
        //   speed: -1,
        //   timestamp: 672652633090.596,
        // }}
        onUserLocationChange={(e) => {
         // log('test', e.nativeEvent.coordinate);
          if(userLocation.latitude === e.nativeEvent.coordinate.latitude && userLocation.longitude === e.nativeEvent.coordinate.longitude)
          {
           // log('identico');
           
          }else{
            //log('ruch');
            setUserLocation(e.nativeEvent.coordinate);
            props.setUserLocation(e.nativeEvent.coordinate);
            setCurrentUserLocation(e.nativeEvent.coordinate);
          }
        }}
        onPanDrag={(e) =>
          props.handleZoom(e.nativeEvent.position.x, e.nativeEvent.position.y)
        }
        showsCompass={true}
        provider={MapView.PROVIDER_GOOGLE}
        customMapStyle={mapStyle}
        // onRegionChange={onRegionChange}
        // onRegionChangeComplete={onRegionChangeComplete}
        zoomEnabled={false}
        scrollEnabled={false}
        showsScale={true}
        rotateEnabled={false}
        followsUserLocation={true}
        showsUserLocation={false}
        mapType={"standard"}
        showsBuildings={false}
      >
        {props.children}
      </MapView>
    </View>
  );
};

export default Map2;
