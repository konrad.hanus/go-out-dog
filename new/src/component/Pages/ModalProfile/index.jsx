import { useState } from "react";
import { Dimensions, StyleSheet } from "react-native";
import ProfileTemplate from "../../Templates/ProfileTemplate";



const ModalProfilePage = ({ showModal, setShowModal, incrementCounterLoaded }) => {
  const [isLoading, setIsLoading] = useState(true);
  return (
   <ProfileTemplate 
   showModal={showModal}
   setShowModal={setShowModal} 
   incrementCounterLoaded={incrementCounterLoaded}
   isLoading={isLoading}
   setIsLoading={setIsLoading}/>
  );
};

export default ModalProfilePage;



