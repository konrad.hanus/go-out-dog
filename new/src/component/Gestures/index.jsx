import React from "react";
import { PanGestureHandler, PinchGestureHandler, LongPressGestureHandler } from "react-native-gesture-handler";
import {
    View,
    StyleSheet,
    Dimensions,
    Pressable
  } from "react-native";
import SCALE from './../../helpers/scale';

  



export default function Gestures(props) {
  const [modalVisible, setModalVisible] = React.useState(false);
  const [size, setSize] = React.useState("md");
  const [tap, setTap] = React.useState(0);
  
  const handleSizeClick = (newSize) => {
    setSize(newSize);
    setModalVisible(!modalVisible);
  };

  
  const handleGesture2 = async (evt) => {
    let { nativeEvent } = evt;
    // console.log(nativeEvent.translationX);
    props.handleZoom(nativeEvent.translationX, nativeEvent.translationY);
    // !isRotating && await
    // handleRotateButton(nativeEvent.translationX);
  };

  const handleGesture = async(evt) => {
      let {nativeEvent} = evt;
    //   console.log('a', nativeEvent.scale);
      
      props.handleZoom(undefined, undefined, nativeEvent.scale+SCALE);
  }
  
  const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get("window");

  
  return (
 // !tap ?
   <PanGestureHandler onGestureEvent={handleGesture2}>
            <PinchGestureHandler onGestureEvent={handleGesture}>
            <Pressable
        
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          zIndex: 1,
          height: SCREEN_HEIGHT,
          width: SCREEN_WIDTH,
        }}
      ><View
              style={{
                // backgroundColor: tap === 0 ? 'blue': "red",
                // opacity: 0.1,
                position: "absolute",
                top: 0,
                left: 0,
                zIndex: 1,
                height: SCREEN_HEIGHT,
                width: SCREEN_WIDTH,
              }}
            >
                {props.children}
            </View></Pressable>
            </PinchGestureHandler>
            </PanGestureHandler>
  //: props.children);
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: "#fff",
    // alignItems: "center",
    // justifyContent: "center",
  },
});
