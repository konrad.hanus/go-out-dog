import { Header, Left, Icon, IconButton, Body, Title, Right, Fab } from "native-base";
import { Entypo, FontAwesome } from "@expo/vector-icons";

const TopArrow = (props) => {
    return (<IconButton icon={<Icon as={Entypo} name="chevron-up" />} borderRadius="full" _icon={{
        color: "orange.500",
        size: "md"
    }} _hover={{
        bg: "orange.600:alpha.20"
    }} _pressed={{
        bg: "orange.600:alpha.20",
        _icon: {
            name: "emoji-flirt"
        },
        _ios: {
            _icon: {
                size: "2xl"
            }
        }
    }} _ios={{
        _icon: {
            size: "2xl"
        }
    }}
        onPress={() => props.zoomIn()}
        style={{
            width: 100,
            height: 100,
            position: "absolute",
            right: 160,
            top: 50,
            zIndex: 99,
        }}
    />);
}

export default TopArrow;