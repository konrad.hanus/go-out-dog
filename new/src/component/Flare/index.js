import { Pressable, Image } from "react-native";
import { Avatar, Text } from "native-base";
import flare from "./flare.png";
import { connect } from "react-redux";

const Flare = (props) => (<Pressable
    onPress={() => props.onPress()}
    style={{
      width: 100,
      height: 120,
      position: "absolute",
      right: 30,
      bottom: 100,
      zIndex: 99,
    }}
  >
    <Image
      style={{
        width: 30, 
        height: 50
      }}
      source={flare}
    ></Image>
    <Avatar
          bg="tertiary.100"
          size="xs"
          style={{
            position: "absolute",
            right: 60,
            bottom: 55,
            zIndex: 99,
            borderWidth: 2,
            borderColor: "#e11d48",
          }}
        >
          <Text style={{color: '#e11d48' }} >{props.locationCounter}</Text>
        </Avatar>
  </Pressable>);
                
export default connect(
    (state) => {
      return {
        locationCounter: state.locationCounter
      }
    }
  )(Flare);

