import { Pressable, Image } from "react-native";
import bag from "./bag.png";
const Bag = (props) => (<Pressable
    onPress={() => props.onPress()}
    style={{
      width: 100,
      height: 100,
      position: "absolute",
      right: 10,
      bottom: 50,
      zIndex: 10,
    }}
  >
    <Image
      style={{
       
        width: 70, 
        height: 70
      }}
      source={bag}
    ></Image>
  </Pressable>);
                

export default Bag;

