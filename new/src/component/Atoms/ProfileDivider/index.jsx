import { View, Text, Divider } from "native-base";
import { StyleSheet } from "react-native";

const ProfileDivider = ({children})=> <View style={styles.taskTitleContainer}>
              <Divider style={styles.divider}>
                <Text style={styles.todayTasksText}>{children}</Text>
              </Divider>
            </View>;

export default ProfileDivider;            

const styles = StyleSheet.create({
  taskTitleContainer: {
    width: "100%",
    paddingTop: 25,
    paddingBottom: 0,
  },
  divider: {
    height: 1,
    backgroundColor: "#9FAEB0",
  },
  todayTasksText: {
    textAlign: "center",
    alignSelf: "center",
    width: 170,
    marginTop: -10,
    height: 20,
    color: "gray",
    fontSize: 11,
    letterSpacing: 1.25,
    backgroundColor: "white",
    textTransform: "uppercase",
    fontWeight: "bold",
  },
});
