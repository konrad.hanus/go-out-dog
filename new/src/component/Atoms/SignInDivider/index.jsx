import { View, Text, Divider } from "native-base";
import { StyleSheet } from "react-native";

const SignInDivider = ({ title }) => (
  <View style={styles.taskTitleContainer}>
    <Divider style={styles.divider}>
      <Text style={styles.todayTasksText}>{title}</Text>
    </Divider>
  </View>
);

export default SignInDivider;

const styles = StyleSheet.create({
  taskTitleContainer: {
    width: "100%",
    paddingTop: 10,
    paddingBottom: 0,
  },
  divider: {
    alignSelf: "center",
    height: 1,
    width: "90%",
    backgroundColor: "#9FAEB0",
  },
  todayTasksText: {
    textAlign: "center",
    alignSelf: "center",
    width: 50,
    marginTop: -10,
    height: 20,
    color: "gray",
    fontSize: 11,
    letterSpacing: 1.25,
    backgroundColor: "white",
    textTransform: "uppercase",
    fontWeight: "bold",
  },
});
