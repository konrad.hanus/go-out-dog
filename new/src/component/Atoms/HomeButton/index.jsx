import { Text } from "react-native";
import { Button } from "native-base";
const HomeButton = (props) => {
  return (
    <Button
      borderRadius={30}
      backgroundColor="#32CD32"
      borderColor="#ffffff"
      borderWidth={2}
      width={props.width}
      height={70}
      onPress={props.onPress}
      style={{ marginBottom: 30 }}
    >
      <Text style={{ fontWeight: "400", fontSize: 20, color: "white" }}>
        {props.title}
      </Text>
    </Button>
  );
};

export default HomeButton;
