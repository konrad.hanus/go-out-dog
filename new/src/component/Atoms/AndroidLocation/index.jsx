import React, { useState, useEffect } from 'react';
import { Platform, Text, View, StyleSheet } from 'react-native';
import * as Location from 'expo-location';
import Page from '../../../pages/Page';
import { Spinner, Center, Box} from "native-base";
export default function App({navigation}) {
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);

  useEffect(() => {
    (async () => {
      
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    })();
  }, []);

  let text = 'Waiting..';
  let isSpinning = true;
  if (errorMsg) {
    text = errorMsg;
    isSpinning = false;
  } else if (location) {
    text = JSON.stringify(location);
    isSpinning = false
    navigation.navigate('Game')
  }

  return (<Page><Center w="100%" h="100%" style={{justifyContent: 'center'}}> <Box safeArea p="2" py="0" w="90%" maxW="290" style={{alignItems: 'center'}}>
    {isSpinning ? <><Spinner color="emerald.500" size="lg" /><Text>Nawiązywanie połączenia z satelitą</Text></> : 
          <Text>{text}</Text>}
    </Box></Center></Page>);
}