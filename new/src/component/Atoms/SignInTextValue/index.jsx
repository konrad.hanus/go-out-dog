import React from "react";
import { Text } from "react-native";

const TextValue = ({ children, color, onPressLink }) => (
  <Text
    style={{
      fontWeight: "400",
      fontSize: 14,
      lineHeight: 22,
      color: color,
    }}
    onPress={onPressLink}
  >
    {children}
  </Text>
);

export default TextValue;
