import {
    OctahedronGeometry,
    Mesh,
    MeshStandardMaterial,
} from "three";

class IconMesh extends Mesh {
    constructor() {
        super(
            new OctahedronGeometry(1.0, 0),
            // new BoxBufferGeometry(1.0, 1.0, 1.0),
            new MeshStandardMaterial({
                // map: new TextureLoader().load(require("./assets/icon.png")),
                color: 0xff0000,
            })
        );
    }
}

export default IconMesh