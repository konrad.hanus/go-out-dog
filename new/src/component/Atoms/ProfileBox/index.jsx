import { Center } from "native-base";

const ProfileBox = ({name, color}) => 
<Center h="100" w="30%" bg={color? color : "primary.300"} rounded="md" shadow={3} fontSize="sm" padding="3" alignContent="center">{name}</Center>    


export default ProfileBox;