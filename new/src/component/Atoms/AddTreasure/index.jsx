import { Pressable, Image } from "react-native";
import { Avatar, Text } from "native-base";
import treasure from "./treasure.png";
import { connect } from "react-redux";

const AddTresure = (props) => (<Pressable
    onPress={() => props.onPress()}
    style={{
      width: 100,
      height: 120,
      position: "absolute",
      left: 10,
      bottom: 110,
      zIndex: 99,
    }}
  >
    <Image
      style={{
        width: 60, 
        height: 60
      }}
      source={treasure}
    ></Image>
    <Avatar
          bg="tertiary.100"
          size="xs"
          style={{
            position: "absolute",
            left: 30,
            bottom: 55,
            zIndex: 99,
            borderWidth: 2,
            borderColor: "green",
          }}
        >
          <Text style={{color: 'green', fontSize: 18 }} >+</Text>
        </Avatar>
  </Pressable>);
                
export default connect(
    (state) => {
      return {
        spadleCounter: 1
      }
    }
  )(AddTresure);

