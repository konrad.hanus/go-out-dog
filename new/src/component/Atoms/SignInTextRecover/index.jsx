import React from "react";
import { View, StyleSheet, Text } from "react-native";

const SignInTextRecover = ({ title }) => (
  <View style={styles.recoverPasswordContainer}>
    <Text style={styles.recoverTitle}>{title}</Text>
  </View>
);

export default SignInTextRecover;

const styles = StyleSheet.create({
  recoverPasswordContainer: {
    justifyContent: "center",
    alignItems: "flex-start",
    alignContent: "center",
    marginLeft: 50,
    marginTop: 8,
  },
  recoverTitle: {
    textAlign: "center",
    fontWeight: "400",
    fontSize: 14,
    lineHeight: 25,
    color: "#38B432",
  },
});
