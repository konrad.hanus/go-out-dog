import { Text } from "react-native";
import { Button } from "native-base";

const WelcomeButton = (props) => <Button
      borderRadius={30}
      backgroundColor={props.bgColor}
      width="100%"
      height={73}
      colorScheme="success"
      marginRight={props.marginRight}
      marginLeft={props.marginLeft}
      onPress={props.onPress}
      style={{ marginBottom: 80 }}
    >
      {props.titleStyle ? (
        <Text style={{ color: "white", fontSize: 19, fontWeight: "400" }}>
          {props.title}
        </Text>
      ) : (
        <Text style={{ color: "black", fontSize: 19, fontWeight: "400" }}>
          {props.title}
        </Text>
      )}
    </Button>
  
export default WelcomeButton;
