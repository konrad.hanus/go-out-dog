import React from "react";
import { View, StyleSheet, Image } from "react-native";
import googleLogo from "../../../assets/png/googleLogo.png";
import appleLogo from "../../../assets/png/appleLogo.png";

const SignInSocialImages = () => (
  <View style={styles.socialImagesContainer}>
    <Image
      source={googleLogo}
      style={{
        width: 28,
        height: 28,
        resizeMode: "contain",
      }}
    />
    <Image
      source={appleLogo}
      style={{
        width: 29,
        height: 28,
        resizeMode: "contain",
      }}
    />
  </View>
);

export default SignInSocialImages;

const styles = StyleSheet.create({
  socialImagesContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    alignContent: "center",
    alignSelf: "center",
    width: "45%",
    marginTop: 40,
    marginBottom: 30,
  },
});
