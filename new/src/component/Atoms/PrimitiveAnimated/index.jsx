import React,{useRef} from 'react'
import { useFrame } from '@react-three/fiber/native'
import {rotation} from '../../../containers/ViewPlayfield';

function PrimitiveAnimated(props) {

  const refPrimitive = useRef(null);
  const data = new Date().getTime();
  
  const {scene, animations, animation, isRotation} = props;

  if(animations && animation)
  {
    let mixer = null;
    mixer = new THREE.AnimationMixer(scene);
    mixer.clipAction(animations[animation]).play();
    useFrame((state, delta) => {
      mixer.update(delta);
      if(isRotation)
      {
        log('rotation', rotation, rotation/50);
        refPrimitive.current.rotation.y = rotation/50;
      }
    });
  }else{
    useFrame((state, delta) => {
      if(isRotation)
      {
        refPrimitive.current.rotation.y += 0.05;
      }
    });
  }

  return <primitive {...props} object={scene} ref={refPrimitive} key={data} />
}

export default PrimitiveAnimated
