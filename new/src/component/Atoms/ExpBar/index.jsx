import { Box, Progress, Center } from "native-base";

const ExpBar = () => {
    return <Center w="100%">
      <Box w="70%" maxW="400">
        <Progress value={45} mx="4" _filledTrack={{
          bg: "lime.500"
        }} />
      </Box>
    </Center>;
  };

  export default ExpBar;