import { Icon } from "native-base";
import Entypo from "react-native-vector-icons/Entypo";

const IconBack = ({ onPress }) => (
  <Icon
    borderRadius={20}
    size="6"
    backgroundColor="coolGray.200"
    as={
      <Entypo
        name="chevron-small-left"
        size={20}
        onPress={onPress}
      />
    }
  ></Icon>
);

export default IconBack;
