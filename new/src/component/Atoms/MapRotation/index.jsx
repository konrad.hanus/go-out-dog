import React, { useRef } from 'react';
import { useFrame } from '@react-three/fiber/native'
import {rotation} from '../../../containers/ViewPlayfield';

const MapRotation = ({children}) => {

    const refPrimitive = useRef(null);
    useFrame((state, delta) => {
      const divider = 60;
      const resultat = rotation/divider;
      // log('rotation', { 'rotation':rotation, 'divider': divider, 'resultat': resultat});
      refPrimitive.current.rotation.y = resultat;   
  });

    return (<mesh ref={refPrimitive}>{children}</mesh>);
}

export default MapRotation;