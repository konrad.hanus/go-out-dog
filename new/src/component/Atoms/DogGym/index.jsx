import PrimitiveAnimated from '../../Atoms/PrimitiveAnimated';
import dogGym from '../../../assets/dogGym.glb';
import { useGLTF } from '@react-three/drei/native'

function DogGym(props) {
    const { scene, animations } = useGLTF(dogGym)
    return <PrimitiveAnimated {...props} scene={scene} animations={animations}  />
  }
  export default DogGym;