import PrimitiveAnimated from '../PrimitiveAnimated';
import dogGym from '../../../assets/dogGym3.glb';
import { useGLTF } from '@react-three/drei/native'

function DogGym(props) {
    const { scene, animations } = useGLTF(dogGym)
    return <PrimitiveAnimated {...props} scene={scene} animations={animations}  />
  }
  export default DogGym;