import WomanGLB from '../../../assets/woman3animated.glb'
import PrimitiveAnimated from '../PrimitiveAnimated';
import { useGLTF } from '@react-three/drei/native'

function Player(props) {
    const { scene, animations } = useGLTF(WomanGLB);

    return <PrimitiveAnimated {...props} scene={scene} animations={animations}  />
    }

export default Player;