import PrimitiveAnimated from '../../Atoms/PrimitiveAnimated';
import ChihuahuaGLB from '../../../assets/chihuahua.glb';
import { useGLTF } from '@react-three/drei/native'

function Chihuahua(props) {
    const { scene, animations } = useGLTF(ChihuahuaGLB)
    return <PrimitiveAnimated {...props} scene={scene} animations={animations}  />
  }
  export default Chihuahua;