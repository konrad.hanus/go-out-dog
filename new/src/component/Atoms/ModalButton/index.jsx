import { Button } from "native-base";
import { StyleSheet } from "react-native";

const ModalButton = (props) => {
  return (
    <Button
      size="lg"
      style={{  
      width: "45%",
      borderRadius: 30,
      height: 42,
      backgroundColor: props.color}}
      onPress={props.onPress}
    >
      {props.title}
    </Button>
  );
};

export default ModalButton;
