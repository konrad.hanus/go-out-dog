import { Box } from "native-base";

const BoxButtons = (props) => (
  <Box safeArea p="2" py="0" w="90%" maxW={props.maxW} style={props.style}>
    {props.children}
  </Box>
);

export default BoxButtons;
