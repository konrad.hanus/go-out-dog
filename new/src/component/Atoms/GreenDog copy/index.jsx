import DogGLB from '../../../assets/dog.glb'
import PrimitiveAnimated from '../PrimitiveAnimated';
import { useGLTF } from '@react-three/drei/native'

function GreenDog(props) {
    const { scene, animations } = useGLTF(DogGLB);
    return <PrimitiveAnimated {...props} scene={scene} animations={animations}  />
    }

export default GreenDog;