import React from "react";
import { View, StyleSheet } from "react-native";
import HomeButton from "../HomeButton";
const SignInButton = ({ title, onPress }) => (
  <View style={styles.logInButtonContainer}>
    <HomeButton title={title} width="80%" onPress={onPress} />
  </View>
);

export default SignInButton;

const styles = StyleSheet.create({
  logInButtonContainer: {
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    marginTop: 20,
  },
});
