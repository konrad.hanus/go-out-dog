import PinczerGLB from '../../../assets/pinczermove.glb';
import PrimitiveAnimated from '../../Atoms/PrimitiveAnimated';
import { useGLTF } from '@react-three/drei/native'

function Pinczer(props) {
    const { scene, animations } = useGLTF(PinczerGLB)
    return <PrimitiveAnimated {...props} scene={scene} animations={animations}  />
  }

export default Pinczer;