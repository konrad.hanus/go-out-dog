import ManGLB from '../../../assets/mananimation.glb'
import PrimitiveAnimated from '../PrimitiveAnimated';
import { useGLTF } from '@react-three/drei/native'

function Player(props) {
    const { scene, animations } = useGLTF(ManGLB);

    return <PrimitiveAnimated {...props} scene={scene} animations={animations}  />
    }

export default Player;