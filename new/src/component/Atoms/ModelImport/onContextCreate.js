import { Renderer, TextureLoader } from "expo-three";
import {
  AmbientLight,
  BoxBufferGeometry,
  OctahedronBufferGeometry,
  OctahedronGeometry,
  Fog,
  GridHelper,
  MeshStandardMaterial,
  PerspectiveCamera,
  PointLight,
  Scene,
  SpotLight,
  AnimationMixer,
  Clock,
} from "three";
import { GLTFLoader } from "./GLTFLoader";
import { rotation } from './../../../containers/ViewPlayfield';

import IconMesh from './../IconMesh';


const onContextCreate = (gl, props, timeout, dateStart) => {
    const { drawingBufferWidth: width, drawingBufferHeight: height } = gl;
  
    // Create a WebGLRenderer without a DOM element
    const renderer = new Renderer({ gl });
    renderer.setSize(width, height);
  
    let camera;
    if(props.isDogProfile)
    {
      camera = new PerspectiveCamera(14, width / height, 0.1, 1000);
      camera.position.set(0, 0, 5);
      camera.scale.set(2,2,2);
      camera.rotation.set(0, 0, 0);
    }else if(props.isDogAvatar)
    {
      console.log('test')
      camera = new PerspectiveCamera(14, width / height, 0.1, 1000);
      camera.position.set(1, 0.5, 9.274);
      camera.scale.set(2,2,2);
      camera.rotation.set(0, 0, 0);
    }else if(props.isPlayerAvatar)
    {
      camera = new PerspectiveCamera(14, width / height, 0.1, 1000);
      camera.position.set(0.87, 0.22, 9.274);
      camera.scale.set(2,2,2);
      camera.rotation.set(0, 0, 0);
    }else{
  
    camera = new PerspectiveCamera(20, width / height, 0.01, 1000);
    camera.position.set(9, 6, -5);
    camera.rotation.set(-122.93, 50.42, 130.25);
  }
  
    const scene = new Scene();
    // scene.fog = new Fog(sceneColor, 1, 10000);
    // scene.add(new GridHelper(10, 10));
  
    const directionalLightRight = new THREE.DirectionalLight( 0xffffff, props.power);
    directionalLightRight.position.set(5, 1, -5);
    scene.add( directionalLightRight );
  
    const directionalLightLeft = new THREE.DirectionalLight( 0xffffff, props.power);
    directionalLightLeft.position.set(5, 1, 5);
    scene.add( directionalLightLeft );
  
    const directionalLightRightBack = new THREE.DirectionalLight( 0xffffff, props.power);
    directionalLightRightBack.position.set(-5, 1, -5);
    scene.add( directionalLightRightBack );
  
    const directionalLightLeftBack = new THREE.DirectionalLight( 0xffffff, props.power);
    directionalLightLeftBack.position.set(-5, 1, 5);
    scene.add( directionalLightLeftBack );
  
    const cube = new IconMesh();
  
  
    if(!props.isPlayerAvatar)
    {
      camera.lookAt(cube.position);
    }
  
    let person;
    
    function update() {
       person.rotation.y += 0.05;
    }
  
    function updateStatic() {
  
      if(props.isRotate)
      {
         person.rotation.y = rotation/50;
      }
  
   }
  
    const loader = new GLTFLoader();
    let mixer;
    let lodaed = false;
    loader.load(props.url,
      function (glb) {
        
        person = glb.scene;
        if(props.player || props.isDogProfile)
        {
          mixer = new AnimationMixer(person);
          const clips = glb.animations;
          console.log(clips)
          const action = mixer.clipAction(clips[props.animation]);
          action.play();
        }
      
        if(props.isDog)
        {
          person.position.set(0, 0, 0.6);
        }else{
          person.position.set(0, 0, 0);
        }
        
        if(props.isDogProfile)
        {
          person.rotation.set(0, 0.45, 0);
          person.position.set(-0.2, -0.2, 0);
        }else{
          person.rotation.set(0, 0, 0);
        }
      
        scene.add(person);
        lodaed = true;
        props.setIsLoading && props.setIsLoading(true);
        if(props.incrementCounterLoaded){
           props.incrementCounterLoaded();
        }else{
          log(`------------------------------------------------------------------------------------------${props.name}: nie ma`);
        }
        const dateEnd = new Date();
        log(`------------------------------------------------------------------------------------------${props.name}: ${(dateEnd-dateStart)/1000}s`);
        // camera.lookAt(person.position);
      },
      function (xhr) {
        //  log((xhr.loader/xhr.total * 100) + "% loaded");
      },
      function (error) {
         log('error', error);
      }
    );
  
    // Setup an animation loop
    const clock = new Clock();
  
    const render = () => {
  
      timeout = requestAnimationFrame(render);
      if(lodaed && props.rotate)
      {
         update();
      }else{
        lodaed && updateStatic();
      }
  
      mixer && mixer.update(clock.getDelta());
      renderer.render(scene, camera);
      gl.endFrameEXP();
    };
    render();

  }

  export default onContextCreate;