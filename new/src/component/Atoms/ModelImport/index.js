import { GLView } from "expo-gl";
import * as React from "react";
import { View, Text } from "react-native";
import onContextCreate from './onContextCreate';
import readAsArrayBuffer from "./helpers/readAsArrayBuffer";

readAsArrayBuffer();

export default function ModelImport(props) {
  let timeout;

  React.useEffect(() => {
    // Clear the animation loop when the component unmounts
    return () => clearTimeout(timeout);
  }, []);

  const dateStart = new Date();
  return (
    <View
      style={props.player ? {
        width:  300,
        height: 300,
        position: 'absolute',
         // borderWidth: 2,
        //  borderColor: "blue",
      }: props.isDogProfile ? {
      width:  200,
      height: 200,
      // borderWidth: 1,
      }
      : {
        width:  300,
        height: 300,
     //   borderWidth: 2,
      }}
    ><GLView
        style={{
          position: 'absolute',
          top: props.isPlayerAvatar || props.isDogProfile ? 0 : props.isDogAvatar ? -60 : 140,
          left: props.isDogAvatar ? -115: 0,
          width:  300,
          height: 300,
          // borderWidth: 4,
          // borderColor: "red",
        }}
        onContextCreate={(gl)=>onContextCreate(gl, props, timeout, dateStart)}
      />
    </View>
  );
}