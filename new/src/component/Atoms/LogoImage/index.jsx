import { Image } from "react-native";
import psiaApkaLogo from "../../../assets/png/LogoTransparent.png";
const Logo = ({ width, height }) => (
  <Image
    source={psiaApkaLogo}
    style={{
      width: width,
      height: height,
      resizeMode: "contain",
    }}
  />
);
export default Logo;
