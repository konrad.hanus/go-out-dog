import {Text} from 'react-native';

const ProfileTextName = ({name}) => <Text style={{fontSize: 32, paddingTop: 10}}>{name}</Text>

export default ProfileTextName;