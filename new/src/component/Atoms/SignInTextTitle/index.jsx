import React from "react";
import { View, StyleSheet, Text } from "react-native";

const SignInTitle = ({ title }) => (
  <View style={styles.logInTextContainer}>
    <Text style={styles.logInTitle}>{title}</Text>
  </View>
);

export default SignInTitle;

const styles = StyleSheet.create({
  logInTextContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
  },
  logInTitle: {
    textAlign: "center",
    fontWeight: "400",
    fontSize: 32,
    lineHeight: 58,
    color: "#383838",
  },
});
