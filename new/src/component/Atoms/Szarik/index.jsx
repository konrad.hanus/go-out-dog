import PrimitiveAnimated from '../../Atoms/PrimitiveAnimated';
import SzarikGLB from '../../../assets/szarikmove.glb';
import { useGLTF } from '@react-three/drei/native'

function Szarik(props) {
    const { scene, animations } = useGLTF(SzarikGLB)
    return <PrimitiveAnimated {...props} scene={scene} animations={animations}  />
  }

export default Szarik;