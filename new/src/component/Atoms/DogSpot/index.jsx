import PrimitiveAnimated from '../../Atoms/PrimitiveAnimated';
import dogSpot from '../../../assets/dogSpot.glb';
import { useGLTF } from '@react-three/drei/native'

function DogSpot(props) {
    const { scene, animations } = useGLTF(dogSpot)
    return <PrimitiveAnimated {...props} scene={scene} animations={animations}  />
  }
  export default DogSpot;