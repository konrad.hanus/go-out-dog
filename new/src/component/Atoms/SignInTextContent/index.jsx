import React from "react";
import { View, StyleSheet } from "react-native";
import TextValue from "../SignInTextValue";

const SignInTextContent = ({ onPressLink, contentNormal, contentLink }) => (
  <View style={styles.helpTextContainer}>
    <TextValue color="#383838">
      {contentNormal}
      <TextValue color="#38B432" onPressLink={onPressLink}>
        {" "}
        {contentLink}{" "}
      </TextValue>
    </TextValue>
  </View>
);
export default SignInTextContent;

const styles = StyleSheet.create({
  helpTextContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    marginBottom: 10,
  },
});
