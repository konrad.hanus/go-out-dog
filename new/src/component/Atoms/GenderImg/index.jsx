import React from "react";
import { Image } from "react-native";

const GenderImage = ({ source }) => (
  <Image
    source={source}
    style={{
      width: 92,
      height: 92,
      resizeMode: "contain",
      margin: 15,
    }}
  />
);

export default GenderImage;
