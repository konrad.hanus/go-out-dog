import { Header, Left, Icon, IconButton, Body, Title, Right, Fab } from "native-base";
import { Entypo, FontAwesome } from "@expo/vector-icons";

const CenterPointer = (props) => {
    return (<IconButton icon={<Icon as={FontAwesome} name="map-marker" />} borderRadius="full" _icon={{
        color: "orange.500",
        size: "md"
      }} _hover={{
        bg: "orange.600:alpha.20"
      }} _pressed={{
        bg: "orange.600:alpha.20",
        _icon: {
          name: "emoji-flirt"
        },
        _ios: {
          _icon: {
            size: "2xl"
          }
        }
      }} _ios={{
        _icon: {
          size: "2xl"
        }
      }} 
      onPress={() => props.setCurrent()}
      style={{
          width: 100,
          height: 100,
          position: "absolute",
          left: 160,
          right: 0,
          top: 130,
          zIndex: 99,
        }}
      />);
}

export default CenterPointer;