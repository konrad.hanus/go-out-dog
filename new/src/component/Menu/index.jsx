import React from 'react';
import { Text, Button,  Modal, ScrollView, Avatar} from "native-base";
import { Pressable, Dimensions } from "react-native";

const Menu = (props) => {
  const [modalVisible, setModalVisible] = React.useState(false);
  const [size, setSize] = React.useState("xl");
  const handleSizeClick = (newSize) => {
    setSize(newSize);
  };

  const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } =
  Dimensions.get("window");

  return (<Pressable
    onPress={() => props.onPress()}
    style={{
      width: 100,
      height: 100,
      position: "absolute",
      left: 170,
      right: "auto",
      bottom: 50,
      zIndex: 99,
    }}
  >
    <Avatar
      bg="primary.300"
      size="lg"
      style={{
        borderWidth: 2,
        borderColor: "#6ee7b7",
        fontSize: 8,
        fontColor: "#262626"
      }}
      source={
        {
          // uri: "https://images.unsplash.com/photo-1607746882042-944635dfe10e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80"
        }
      }
    >{props.label}</Avatar>
<Modal isOpen={modalVisible} onClose={setModalVisible} size={'full'}>
                    <Modal.Content height={SCREEN_HEIGHT}>
                    <Modal.CloseButton />
                    <Modal.Header>Witaj w Psiej Apce</Modal.Header>
                    <Modal.Body >
                         <ScrollView>
                        <Text>
                           Tutaj znajdziesz będziesz mógł edytować swój profil i dodawać znajomych.
                        </Text>
                        </ScrollView> 
                    </Modal.Body>
                    <Modal.Footer>
                        <Button.Group space={2}>
                        <Button variant="ghost" colorScheme="blueGray" onPress={() => {
                        setModalVisible(false);
                        }}>Zamknij</Button>
                        <Button onPress={() => {
                        setModalVisible(false);
                        }}>Ok rozumiem</Button>
                        </Button.Group>
                    </Modal.Footer>
                    </Modal.Content>
                </Modal>
                </Pressable>);
                }
                

export default Menu;