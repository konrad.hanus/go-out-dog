import { Pressable, Image } from "react-native";
import { Avatar, Text } from "native-base";
import spadle from "./spadle.png";
import { connect } from "react-redux";

const Spadle = (props) => (<Pressable
    onPress={() => props.onPress()}
    style={{
      width: 100,
      height: 120,
      position: "absolute",
      right: -25,
      bottom: 100,
      zIndex: 99,
    }}
  >
    <Image
      style={{
        width: 60, 
        height: 60
      }}
      source={spadle}
    ></Image>
    <Avatar
          bg="tertiary.100"
          size="xs"
          style={{
            position: "absolute",
            right: 60,
            bottom: 55,
            zIndex: 99,
            borderWidth: 2,
            borderColor: "#e11d48",
          }}
        >
          <Text style={{color: '#e11d48' }} >{props.spadleCounter}</Text>
        </Avatar>
  </Pressable>);
                
export default connect(
    (state) => {
      return {
        spadleCounter: 1
      }
    }
  )(Spadle);

