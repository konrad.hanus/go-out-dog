import { Pressable, Image } from "react-native";
import { Avatar } from "native-base";
import pin from "./pin.png";
import dog from "./dog.jpeg";

const Pin = (props) => {
  return (
    <Pressable
      onPress={() => props.onPress()}
      style={{
        width: 100,
        height: 100,
        //  position: "absolute",
        // left: 70,
        // bottom: 60,
        zIndex: 99999,
      }}
    >
      <Image
        source={props.image ? props.image : dog}
        style={{
          width: 50,
          height: 50,
          position: "absolute",
          left: 25,
          top: 10,
        }}
      ></Image>
      <Image
        source={pin}
        style={{ width: 100, height: 100, position: "absolute" }}
      ></Image>
    </Pressable>
  );
};

export default Pin;
