import { View, Text } from "native-base";
import ProfileRow from "../../Molecules/ProfileRow";
import ProfileDivider from "../../Atoms/ProfileDivider";

const ProfileTiles = ({tiles, title}) => {
  return (
    <><ProfileDivider>{title}</ProfileDivider>
      <View style={{ marginTop: 15 }}>
        <ProfileRow first={tiles[0]} second={tiles[1]} third={tiles[2]} />
        <ProfileRow first={tiles[3]} second={tiles[4]} third={tiles[5]} />
        <ProfileRow first={tiles[6]} second={tiles[7]} third={tiles[8]} />
      </View>
    </>
  ); 
};

export default ProfileTiles;
