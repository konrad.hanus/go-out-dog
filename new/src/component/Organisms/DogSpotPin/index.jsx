// import DogSpot from "../../Molecules/DogSpot";
import DogSpot from "../../Molecules/DogSpot/index_new";
import MapView from "react-native-maps";
// import DogSpot from "../../Atoms/DogSpot";
 
const DogSpotPin = ({dogSpots, removeDogSpotLocation, setCoutnerLoaded, incrementCounterLoaded, setIsLoading}) => {

  log('ds', dogSpots);

    return dogSpots &&
    Object.keys(dogSpots).map((key, index) => {
      // log('i', index)
      const location = dogSpots[key].location;
      return ( index === 3 ) && (
        location && <MapView.Marker
        key={`map-view-marker-dog-spotsc-component-${index}`}
          onPress={() => {
            // removeDogSpotLocation(key);
            // alert('To jest skarb, dodany przez '+ props.treasures[key].who);
          }}
          coordinate={location}
        >
           <DogSpot /> 
           {/* <DogSpot setIsLoading={setIsLoading} setCoutnerLoaded={setCoutnerLoaded} incrementCounterLoaded={incrementCounterLoaded}/>  */}
        </MapView.Marker>
      );
    });
}

export default DogSpotPin