// import DogSpot from "../../Molecules/DogSpot";
import MapView from "react-native-maps";

const DogSpotPin = ({dogSpots, removeDogSpotLocation, setCoutnerLoaded, incrementCounterLoaded, setIsLoading}) => {
    return dogSpots &&
    Object.keys(dogSpots).map((key, index) => {
      const location = dogSpots[key].location;
      return (
        location && <MapView.Marker
        key={`map-view-marker-dog-spotsc-component-${index}`}
          onPress={() => {
            removeDogSpotLocation(key);
            // alert('To jest skarb, dodany przez '+ props.treasures[key].who);
          }}
          coordinate={location}
        >
          {/* <DogSpot setIsLoading={setIsLoading} setCoutnerLoaded={setCoutnerLoaded} incrementCounterLoaded={incrementCounterLoaded}/> */}
        </MapView.Marker>
      );
    });
}

export default DogSpotPin