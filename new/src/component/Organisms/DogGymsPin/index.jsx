import DogGym from "../../Molecules/DogGym";
import MapView from "react-native-maps";

const DogGymsPin = ({dogGyms, removeDogGymLocation, incrementCounterLoaded}) => {
    return dogGyms &&
    Object.keys(dogGyms).map((key, index) => {
      const location = dogGyms[key].location;
      return (
        location && <MapView.Marker
        key={`map-view-marker-dog-gyms-component-${index}`}
          onPress={() => {
            removeDogGymLocation(key);
          }}
          coordinate={location}
        >
          <DogGym incrementCounterLoaded={incrementCounterLoaded}/>
        </MapView.Marker>
      );
    });
}

export default DogGymsPin


    