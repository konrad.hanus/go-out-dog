import Treasures from "../../Molecules/Treasures";
import MapView from "react-native-maps";

const TreasuresPin = ({treasures, removeTreasureLocation, incrementCounterLoaded}) => {
    return treasures &&
    Object.keys(treasures).map((key, index) => {
      const location = treasures[key].location;
      return (
        location && <MapView.Marker
          key={`map-view-marker-treasures-component-${index}`}
          onPress={() => {
            removeTreasureLocation(key);
            // alert('To jest skarb, dodany przez '+ props.treasures[key].who);
          }}
          coordinate={location}
        >
          <Treasures setCoutnerLoaded={setCoutnerLoaded} incrementCounterLoaded={incrementCounterLoaded}/>
        </MapView.Marker>
      );
    });
}

export default TreasuresPin