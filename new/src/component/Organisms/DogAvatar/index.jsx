
import {Pressable, View} from 'react-native';
import DogAvatar3D from '../../Molecules/DogAvatar';

const DogAvatar = (props) => {
    return (<Pressable
        onPress={() => props.onPress()}
        style={{
          width: 100,
          height: 100,
           position: "absolute",
          left: 80,
          bottom: 25,
          zIndex: 10,
        }}
      >
        <View style={{borderColor: 'green', borderWidth: 4, borderRadius: 100, height: 50, width: 50, overflow: 'hidden', backgroundColor: 'black'}}>
          <DogAvatar3D incrementCounterLoaded={props.incrementCounterLoaded}/>
        </View>
      </Pressable>);
}

export default DogAvatar;
