import { StyleSheet } from "react-native";
import { View, Text } from "native-base";
import TaskStar from "../TaskStar";


export default TaskBox = (props) => {
  const ThreeStars = Array.from({ length: 3 }, (_, index) => (
    <TaskStar key={index} />
  ));

  return (
    <View style={styles.iconWithContentContainer}>
      {/* <Text>{props.iconName}2</Text> */}
      {/* <MaterialIcon name='star' /> */}
      {props.icon}

      <View
        style={
          props.isGreenBg ? styles.taskContainer : styles.taskContainerWhite
        }
      >
        <View
          style={
            props.isSpace
              ? styles.contentWithStars
              : styles.contentWithStarsWhite
          }
        >
          <View>
            {props.isSingleTitle ? (
              <Text style={styles.taskTitleText}>{props.titleTextOne}</Text>
            ) : (
              <>
                <Text style={styles.taskTitleText}>{props.titleTextOne}</Text>
                <Text style={styles.taskTitleText}>{props.titleTextTwo}</Text>
              </>
            )}
          </View>
          <View style={styles.imageContainer}>{ThreeStars}</View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  iconWithContentContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    alignContent: "center",
    width: "100%",
    borderColor: 'red',
  },
  taskContainer: {
    backgroundColor: "#F4FCEE",
    height: 39,
    borderRadius: 15,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
  },
  taskContainerWhite: {
    backgroundColor: "white",
    height: 39,
    borderRadius: 15,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
  },
  contentWithStars: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    alignContent: "center",
    width: "100%",
  },
  contentWithStarsWhite: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    alignContent: "center",
    width: "87%",
  },
  taskTitleText: {
    fontSize: 11,
  },
  imageContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
  },
});
