import FontAwesome from 'react-native-vector-icons/FontAwesome';

const TaskStar = () => {
  return (
    <FontAwesome name="star-o" size={25} style={{color: 'grey', paddingRight: 5}} />
  );
};

export default TaskStar;
