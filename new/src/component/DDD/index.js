// import React from "react";
// import { View } from "react-native";
// import Expo from "expo";
// import {
//   Scene,
//   Mesh,
//   MeshBasicMaterial,
//   PerspectiveCamera,
//   BoxBufferGeometry,
//   Geometry,
// } from "three";
// import ExpoTHREE, { Renderer } from "expo-three";
// import { ExpoWebGLRenderingContext, GLView } from "expo-gl";

// const DDD = (props) => {
//   const onContextCreate = async (gl) => {
//     const scene = new Scene();
//     const camera = new PerspectiveCamera(
//       75,
//       gl.drawingBufferWidth / gl.drawingBufferHeight,
//       0.1,
//       1000
//     );

//     gl.canvas = {
//       width: gl.drawingBufferWidth,
//       height: gl.drawingBufferHeight,
//     };

//     camera.position.z = 2;

//     const renderer = new ExpoTHREE.Renderer({ gl });
//     renderer.setSize(gl.drawingBufferWidth, gl.drawingBufferHeight);

//     const geometry = new BoxBufferGeometry(1, 1, 1);
//     const material = new MeshBasicMaterial({
//       color: "blue",
//     });

//     const cube = new Mesh(geometry, material);
//     scene.add(cube);

//     const render = () => {
//       requestAnimationFrame(render);
//       cube.rotation.x += 0.01;
//       cube.rotation.y += 0.01;
//       renderer(scene, camera);
//       gl.endFrameEXP();
//     };

//     render();
//   };
//   return (
//     <View>
//       <GLView
//         onContextCreate={onContextCreate}
//         style={{ width: 500, height: 500 }}
//       ></GLView>
//     </View>
//   );
// };

// export default DDD;
// -------------------------------------------------
// import React from "react";
// import { View } from "react-native";
// import { GLView } from "expo-gl";

// export default function App() {
//   return (
//     <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
//       <GLView
//         style={{ width: 300, height: 300 }}
//         onContextCreate={onContextCreate}
//       />
//     </View>
//   );
// }

// function onContextCreate(gl) {
//   gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
//   gl.clearColor(0, 1, 1, 1);

//   // Create vertex shader (shape & position)
//   const vert = gl.createShader(gl.VERTEX_SHADER);
//   gl.shaderSource(
//     vert,
//     `
//     void main(void) {
//       gl_Position = vec4(0.0, 0.0, 0.0, 1.0);
//       gl_PointSize = 150.0;
//     }
//   `
//   );
//   gl.compileShader(vert);

//   // Create fragment shader (color)
//   const frag = gl.createShader(gl.FRAGMENT_SHADER);
//   gl.shaderSource(
//     frag,
//     `
//     void main(void) {
//       gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
//     }
//   `
//   );
//   gl.compileShader(frag);

//   // Link together into a program
//   const program = gl.createProgram();
//   gl.attachShader(program, vert);
//   gl.attachShader(program, frag);
//   gl.linkProgram(program);
//   gl.useProgram(program);

//   gl.clear(gl.COLOR_BUFFER_BIT);
//   gl.drawArrays(gl.POINTS, 0, 1);

//   gl.flush();
//   gl.endFrameEXP();
// }
// -------------------------------
import { ExpoWebGLRenderingContext, GLView } from "expo-gl";
import { Renderer, TextureLoader } from "expo-three";
import * as React from "react";
import {
  AmbientLight,
  BoxBufferGeometry,
  OctahedronBufferGeometry,
  OctahedronGeometry,
  Fog,
  GridHelper,
  Mesh,
  MeshStandardMaterial,
  PerspectiveCamera,
  PointLight,
  Scene,
  SpotLight,
} from "three";

export default function App() {
  let timeout;

  React.useEffect(() => {
    // Clear the animation loop when the component unmounts
    return () => clearTimeout(timeout);
  }, []);

  return (
    <GLView
      style={{ position: "aboslute", top: 0, width: 200, height: 200 }}
      onContextCreate={async (gl) => {
        const { drawingBufferWidth: width, drawingBufferHeight: height } = gl;
        const sceneColor = 0x6ad6f0;

        // Create a WebGLRenderer without a DOM element
        const renderer = new Renderer({ gl });
        renderer.setSize(width, height);
        // renderer.setClearColor(sceneColor);

        const camera = new PerspectiveCamera(70, width / height, 0.01, 1000);
        camera.position.set(2, 5, 5);

        const scene = new Scene();
        // scene.fog = new Fog(sceneColor, 1, 10000);
        // scene.add(new GridHelper(10, 10));

        const ambientLight = new AmbientLight(0x101010);
        scene.add(ambientLight);

        const pointLight = new PointLight(0xC70039, 2, 1000, 1);
        pointLight.position.set(0, 200, 200);
        scene.add(pointLight);

        const spotLight = new SpotLight(0xC70039, 0.5);
        spotLight.position.set(0, 500, 100);
        spotLight.lookAt(scene.position);
        scene.add(spotLight);

        const cube = new IconMesh();

        scene.add(cube);

        camera.lookAt(cube.position);

        function update() {
          cube.rotation.y += 0.05;
          // cube.rotation.x += 0.025;
        }

        // Setup an animation loop
        const render = () => {
          timeout = requestAnimationFrame(render);
          update();
          renderer.render(scene, camera);
          gl.endFrameEXP();
        };
        render();
      }}
    />
  );
}

class IconMesh extends Mesh {
  constructor() {
    super(
      new OctahedronGeometry(1.0, 0),
      // new BoxBufferGeometry(1.0, 1.0, 1.0),
      new MeshStandardMaterial({
        map: new TextureLoader().load(require("./assets/icon.png")),
        // color: 0xff0000
      })
    );
  }
}
