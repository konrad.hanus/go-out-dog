import { View, Image } from "react-native";

const Horizont = (props) => {
  return (
    <View
      style={{
        height: 200,
        width: 1000,
        position: "absolute",
        top: -230,
      }}
    >
      <Image
        source={require("./smog.png")}
        resizeMode="stretch"
        style={{ width: 1000, left: -100 }}
      />
    </View>
  );
};

export default Horizont;
