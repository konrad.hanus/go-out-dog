export const FETCH_DOG_GYMS_LOCATION = '@@FETCH/DOG_GYMS_LOCATION';
export const PUT_DOG_GYMS_LOCATION = '@@PUT/DOG_GYMS_LOCATION';
export const REMOVE_DOG_GYM_LOCATION = '@@PUT/REMOVE_DOG_GYM_LOCATION';

export const actionCreators = {
    fetchDogGymsLocation: () => {
        return ({
            type: FETCH_DOG_GYMS_LOCATION,
        })
    },  
    putDogGymsLocation: (dogGym) => {
        return ({
            type: PUT_DOG_GYMS_LOCATION,
            payload: dogGym
        })
    },
    removeDogGymLocation: (id) => {
        return ({
            type: REMOVE_DOG_GYM_LOCATION, 
            meta: id,
        })
    }  
};