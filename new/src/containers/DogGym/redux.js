
import { PUT_DOG_GYMS_LOCATION } from './action';

const initiaState = []

export function dogGymsReducer(state=initiaState, action){
    switch (action.type) {

        case PUT_DOG_GYMS_LOCATION:
          return action.payload;
    
        default:
          return state;
    };
   }
   
   export default dogGymsReducer;