import React, { Component } from 'react';
import {
    Content,
    Text,
    List, ListItem, Body,
    Container, Header, Item, Input, Icon, Button, Right
} from 'native-base';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { actionCreators } from './action';
import { connect } from 'react-redux';
import IconFoundation from 'react-native-vector-icons/Foundation';
import { actionCreators as ownersActionCreators } from '../Owners/action';
import PAGE from '../../pages/pages';

class Keepers extends Component {

    onClickRemoveKeeper = (keeper) => {
        this.props.removeUsersKeeper(keeper);
    }

    onClickRemoveOwner = (keeper) => {
        this.props.removeUsersOwner(keeper);
    }

    render() {
        return (
            <Content>
                <Container>
                    
                <ListItem itemDivider>
                        <Text>Współwłaścicele psa</Text>
                    </ListItem>
                    <List>
                        {this.props.owners.map((owner, id) => <ListItem key={id}>
                            <Body><Text>{owner.email}</Text></Body>

                            <Right><Button small danger onPress={() => {
                                this.onClickRemoveOwner(owner);
                            }}><Text><IconFoundation name="guide-dog" size={22} /></Text></Button></Right>
                        </ListItem>)
                        }
                    </List>
                    <List>
                    <ListItem itemDivider>
                        <Text>Opiekunowie zastępczy</Text>
                    </ListItem>
                        {this.props.keepers.map((keeper, id) => <ListItem key={id}>
                            <Body><Text>{keeper.email}</Text></Body>

                            <Right><Button small danger onPress={() => {
                                this.onClickRemoveKeeper(keeper);
                            }}><Text><IconFontAwesome5 name="user-minus" size={15} /></Text></Button></Right>
                        </ListItem>)
                        }
                    </List>
                    <Body>
                        <Button transparent onPress={() => this.props.navigation.navigate(PAGE.SEARCH)}>
                            <Text>Szukaj znajomych</Text>
                        </Button>
                    </Body>
                </Container>

                {/* <Body>
                <Text> </Text>
                    <Text>Twój unikatowy nr:</Text>
                    <Text>{this.props.uid}</Text>
                    <Text> </Text>
                    <Image source={{uri: `https://api.qrserver.com/v1/create-qr-code/?size=250x250&data=${this.props.uid}`}} style={{height: 250, width: 250, flex: 1}}/>
                    <Text> </Text>
                    <Text>Udostępnij go znajomemu</Text>
                    <Text>z którą chcesz się połączyć</Text>
                    
                </Body> */}

            </Content>
        );
    }
}

export default connect(
    ((state) => {
        return ({
            uid: state.user && state.user.user.uid && state.user.user.uid,
            keepers: state.keepers,
            owners: state.owners
        })
    }), { ...actionCreators, ...ownersActionCreators }
)(Keepers);




