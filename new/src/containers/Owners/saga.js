import { takeEvery, call, put, all, fork, select } from 'redux-saga/effects';
import { ADD_OWNER_TO_USER, FETCH_USERS_OWNER, REMOVE_USERS_OWNER } from './action';
import { actionCreators } from './action';
import { reduxSagaFirebase } from '../Walk/saga';
import { userId, userEmail } from '../../selectors/user';

function* onAddOwnerToUserSaga(action) {
    const getUserId = yield select(userId);
    const getUserEmail = yield select(userEmail);
    const owner = action.payload;
    yield call(reduxSagaFirebase.database.update, getUserId + '/owners/' + owner.id, { ...owner, order: 1 });
    yield call(reduxSagaFirebase.database.update, owner.id + '/owners/' + getUserId, {
        email: getUserEmail,
        id: getUserId,
        order: 2
    });
    yield put(actionCreators.fetchAllOwnersOfUser());
}

function* onRemoveOwnerFromUserSaga(action) {
    const getUserId = yield select(userId);
    const owner = action.payload;
    yield call(reduxSagaFirebase.database.delete, getUserId + '/owners/' + owner.id);
    yield call(reduxSagaFirebase.database.delete, owner.id + '/owners/' + getUserId);
    yield put(actionCreators.fetchAllOwnersOfUser());
}

function* onFetchAllUsersOwnersSaga() {
    const getUserId = yield select(userId);
    const allUsersOwners = yield call(reduxSagaFirebase.database.read, getUserId + '/owners/');

    if (allUsersOwners) {
        const allUsersOwnersArray = Object.values(allUsersOwners);
        yield put(actionCreators.putAllUsersOwners(allUsersOwnersArray));
    } else {
        yield put(actionCreators.putAllUsersOwners([]));
    }
}

export function* addOwnerToUserSaga() {
    yield takeEvery(ADD_OWNER_TO_USER, onAddOwnerToUserSaga);
}

export function* fetchAllUsersOwnersSaga() {
    yield takeEvery(FETCH_USERS_OWNER, onFetchAllUsersOwnersSaga);
}

export function* removeOwnerFromUserSaga() {
    yield takeEvery(REMOVE_USERS_OWNER, onRemoveOwnerFromUserSaga);
}

export function* ownersSaga() {
    yield all([fork(addOwnerToUserSaga), fork(fetchAllUsersOwnersSaga), fork(removeOwnerFromUserSaga)]);
}
