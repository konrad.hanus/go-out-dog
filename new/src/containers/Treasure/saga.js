import { takeEvery, call, put, all, fork, select } from 'redux-saga/effects';
import { FETCH_TREASURES_LOCATION, REMOVE_TREASURE_LOCATION, actionCreators } from './action';
import { reduxSagaFirebase } from '../Walk/saga';

function* onFetchTreasuresLocation() {
    const allTreasure = yield call(reduxSagaFirebase.database.read, '/public/treasure/');
    yield put(actionCreators.putTreasuresLocation(allTreasure));
}

function* onRemoveTreasureLocation(action) {
    const allTreasure = yield call(reduxSagaFirebase.database.delete, '/public/treasure/'+action.meta);
    alert('skarb usunięto');
    yield put(actionCreators.fetchTreasuresLocation());
}

export function* fetchTreasuresLocation() {
    yield takeEvery(FETCH_TREASURES_LOCATION, onFetchTreasuresLocation);
}

export function* removeTreasureLocation() {
    yield takeEvery(REMOVE_TREASURE_LOCATION, onRemoveTreasureLocation);
}

export function* treasuresLocation() {
    yield all([fork(fetchTreasuresLocation), fork(removeTreasureLocation)]);
}
