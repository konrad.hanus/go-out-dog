
import { PUT_TREASURES_LOCATION } from './action';

const initiaState = []

export function treasuresReducer(state=initiaState, action){
    switch (action.type) {

        case PUT_TREASURES_LOCATION:
          return action.payload;
    
        default:
          return state;
    };
   }
   
   export default treasuresReducer;