export const FETCH_TREASURES_LOCATION = '@@FETCH/TREASURES_LOCATION';
export const PUT_TREASURES_LOCATION = '@@PUT/TREASURES_LOCATION';
export const REMOVE_TREASURE_LOCATION = '@@PUT/REMOVE_TREASURE_LOCATION';

export const actionCreators = {
    fetchTreasuresLocation: () => {
        return ({
            type: FETCH_TREASURES_LOCATION,
        })
    },  
    putTreasuresLocation: (friends) => {
        return ({
            type: PUT_TREASURES_LOCATION,
            payload: friends
        })
    },
    removeTreasureLocation: (id) => {
        return ({
            type: REMOVE_TREASURE_LOCATION, 
            meta: id,
        })
    }  
};