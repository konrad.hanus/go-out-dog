export const FETCH_FRIENDS_LOCATION = '@@FETCH/FRIENDS_LOCATION';
export const PUT_FRIENDS_LOCATION = '@@PUT/FRIENDS_LOCATION';

export const actionCreators = {
    fetchFriendsLocation: () => {
        return ({
            type: FETCH_FRIENDS_LOCATION,
        })
    },  
    putFriendsLocation: (friends) => {
        return ({
            type: PUT_FRIENDS_LOCATION,
            payload: friends
        })
    },  
};