import { takeEvery, call, put, all, fork, select } from 'redux-saga/effects';
import { FETCH_FRIENDS_LOCATION, actionCreators } from './action';
import { reduxSagaFirebase } from '../Walk/saga';
import * as Device from 'expo-device';


function* onFetchFriendsLocation() {
    const allFriends = yield call(reduxSagaFirebase.database.read, '/public/players/');
    yield put(actionCreators.putFriendsLocation(allFriends));
}

export function* fetchFriendsLocation() {
    yield takeEvery(FETCH_FRIENDS_LOCATION, onFetchFriendsLocation);
}

export function* friendsLocation() {
    yield all([fork(fetchFriendsLocation)]);
}
