import React, { useState, useRef } from "react";
// import { StatusBar } from "expo-status-bar";
import { StyleSheet, Image, Text, View /* ActivityIndicator, Pressable */ } from "react-native";
import Map2 from "../../component/Map2";
// import { Button, Modal, FormControl, Input, Center, NativeBaseProvider } from "native-base";
import ProfileAvatar from "../../component/ProfileAvatar";
import DogAvatar from "../../component/Organisms/DogAvatar";
// import Menu from "../../component/Menu";
import Bag from "../../component/Bag";
// import { PanGestureHandler } from "react-native-gesture-handler";
import Gestures from "../../component/Gestures";
import MapView  /*, { Circle, Polygon } */ from "react-native-maps";
// import Pin from "../../component/Pin";
// import Doge from "../../component/Pin/Doge.jpeg";
// import LargeMap from "../../component/LargeMap";
// import SCALE from "../../helpers/scale";
import flare2 from "../../component/Flare/flare2.png";
import { connect } from "react-redux";
import { actionCreators } from "../MyLocation/action";
import { actionCreators as actionCreatorsFriends } from "../MyFriends/action";
import { actionCreators as actionCreatorsTreasures } from "../Treasure/action";
import { actionCreators as actionCreatorsDogSpots } from "../DogSpot/action";
import { actionCreators as actionCreatorsDogGyms } from "../DogGym/action";
import Flare from "../../component/Flare";
import Spadle from "../../component/Spadle";
// import ThreeJs from "../../component/ThreeJs";
// import ViewMap from "../ViewMap";
// import DogGym from "../../component/Molecules/DogGym";
// import DogSpot from "../../component/Molecules/DogSpot";
import Player from "../../component/Molecules/Player";
// import bag from "./../../component/Bag/bag.png";
import LoadingScreen from '../../component/Molecules/LoadingScreen';
import LoadingSpinner from '../../component/Molecules/LoadingSpinner';
import LoadingText from "../../component/Molecules/LoadingText";

// import Dog from './../../component/Molecules/Dog/index_new';
import ModalProfile from "../../component/Pages/ModalProfile";
import Builder from "../../component/Molecules/Builder";
import LoadingProgressBar from '../../component/Molecules/LoadingProgressBar';
import DogMap from "./../../component/Molecules/Dog/index_new";

export let rotation = 0;

const DogApp = (props) => {
  const [showFlare, setShowFlare] = useState(false);
  const [showItemInBag, setShowItemInBag] = useState(false);

  const [builder, setBuilder] = useState(false);
  const [showModal, setShowModal] = useState(false);


  const fireFlare = () => {
    setShowFlare(true);

    setTimeout(() => {
      setShowFlare(false);
    }, 30000);
  };

  const mapRef = useRef(null);
  const mapRefLarge = useRef(null);
  const [userLocation, setUserLocation] = useState({ latitude: 50.9215267, longitude: 15.7563365 });
  const [sharedCamera, setSharedCamera] = useState();
  const [cameraRotation, setCameraRotation] = useState();
  const [isSet, setIsSet] = useState(false);
  const [heading, setHeading] = useState(0);
  const setUserLocationWrapper = (location) => {
   if(!isSet){
      log('setUserLocation', location);
      setUserLocation(location);
      setIsSet(true)
    }
  }

  const handleZoom = async (heading, angle, scale, duration) => {
    const camera = mapRef && await mapRef.current.getCamera();

    mapRef.current.animateCamera(
      {
        center: {
          latitude: userLocation.latitude ? userLocation.latitude : 0,
          longitude: userLocation.longitude ? userLocation.longitude : 0,
        },
        heading: heading ? heading : camera.heading,
        pitch: 10,
        zoom: scale, //? scale : camera.scale,
        // pitch: angle ? (-angle > 20 ? -angle : 15) : camera.angle, //angle
        pitch: 65,
      },
      { duration: duration ? duration : 20 }
    );
    rotation = camera.heading;
  

  };
  const coordinate = { latitude: 51.086781129189376, longitude: 17.05304514781193 };
   
  return (
    <>
      <Gestures handleZoom={handleZoom}>
      <>
        {builder && (
          <Builder
            setMyLocation={props.setMyLocation}
            fetchDogsGymLocation={props.fetchDogsGymLocation}
            fetchDogsSpotLocation={props.fetchDogsSpotLocation}
            fetchTreasuresLocation={props.fetchTreasuresLocation}
            userLocation={userLocation}
          />
        )}

        {/* <Menu
          label={"MAPA"}
          onPress={() => {
            props.fetchFriendsLocation();
            props.setPage("2");
          }}
        /> */}
        {showItemInBag && (
          <>
            <Spadle
              onPress={() => {
                alert(
                  "Uzyj szpadla w miejscu gdzie bedziesz chciał odkopać skarb"
                );
              }}
            />

            {props.locationCounter !== 0 && (
              <Flare
                onPress={() => {
                  if (!showFlare) {
                    props.setMyLocation(userLocation);
                    fireFlare();
                  }
                }}
              />
            )}
          </>
        )}

        {/* {props.isLoading && <LoadingScreen />}
        {props.isLoading && <LoadingSpinner />}
        {props.isLoading && <LoadingProgressBar sumItems={props.sumItems} nextI={props.nextI} />}
        {props.isLoading && <LoadingText />} */}
        <Bag
          onPress={() => {
            setShowItemInBag(!showItemInBag);
          }}
        />
      </>
      </Gestures>

      <Map2
        mapRef={mapRef}
        setUserLocation={setUserLocationWrapper}
        userLocation={userLocation}
        handleZoom={handleZoom}
        sharedCamera={sharedCamera}
        setSharedCamera={setSharedCamera}
      >
        {props.children}
      </Map2>

      <View style={{borderWidth: 1,
        //  backgroundColor: 'silver', 
         position: "absolute", top: 0, left:0, right:0, bottom: 0}}>
        <DogMap 
          mapRef={mapRef} 
          userLocation={userLocation} 
          dogSpots={props.dogSpots} 
          dogGyms={props.dogGyms}
        />
      </View>
      <ProfileAvatar
        onPress={() => {
          // setBuilder(!builder);
        }}
        distance={0}
        userLocation={userLocation}
        camera={cameraRotation}
        incrementCounterLoaded={props.incrementCounterLoaded}
      />
      <DogAvatar
        onPress={() => {
          log('show modal');
          setShowModal(true);
        }}

        incrementCounterLoaded={props.incrementCounterLoaded}
      />
       <ModalProfile showModal={showModal} setShowModal={setShowModal} incrementCounterLoaded={props.incrementCounterLoaded} /> 
    </>
  );
};

export default connect(
  (state) => {
    return {
      friends: state.friends,
      locationCounter: state.locationCounter,
      treasures: state.treasures,
      dogSpots: state.dogSpots,
      dogGyms: state.dogGyms,
    };
  },
  {
    setMyLocation: actionCreators.putMyLocation,
    fetchFriendsLocation: actionCreatorsFriends.fetchFriendsLocation,
    fetchTreasuresLocation: actionCreatorsTreasures.fetchTreasuresLocation,
    removeTreasureLocation: actionCreatorsTreasures.removeTreasureLocation,
    removeDogSpotLocation: actionCreatorsDogSpots.removeDogSpotLocation,
    fetchDogsSpotLocation: actionCreatorsDogSpots.fetchDogSpotsLocation,
    removeDogGymLocation: actionCreatorsDogGyms.removeDogGymLocation,
    fetchDogsGymLocation: actionCreatorsDogGyms.fetchDogGymsLocation,
  }
)(DogApp);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
