import React, { Component } from 'react';
import { Text, Content, Grid, Col, View } from 'native-base';
import { connect } from 'react-redux';
class SwapHeader extends Component {


    renderRed = (counter) => {

        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'flex-start',
            }}><Grid>
                    {Array.from({ length: counter }, (_, k) => (
                        <Col style={{ backgroundColor: '#ce3c3e', height: 10, width: 10, marginRight: 5, }} key={k}></Col>
                    ))}
                </Grid>
            </View>
        )
    } 

    renderGreen = (counter) => {

        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'flex-end',
            }}><Grid>
                    {Array.from({ length: counter }, (_, k) => (
                        <Col style={{ backgroundColor: '#4dad4a', height: 10, width: 10, marginRight: 5, }} key={k}></Col>
                    ))}
                </Grid>
            </View>
        )
    }

    render() {
        return (
            <>
                {this.renderRed(this.props.counterRed)}
                {this.renderGreen(this.props.counterGreen)}
            </>
        );
    }
}


export default connect(
    ((state) => {


        function isMySwapWalk(walk) {
            if (walk.swap) {
                return walk.swap?.oryginal === state.user.user.uid;
            } else {
                return false;
            }
        }

        function isCoOwnerSwapWalk(walk) {

            const coOwnerId = state.owners && state.owners[0] ? state.owners[0].id : null;
            if (walk.swap) {

                if (walk.swap.oryginal === coOwnerId) {


                }

                return walk.swap.oryginal === coOwnerId;
            } else {
                return false;
            }
        }

        const counterRed = state.walks.filter(isMySwapWalk).length;
        const counterGreen = state.walks.filter(isCoOwnerSwapWalk).length;

        const diff = counterGreen - counterRed

        return ({
            counterGreen: diff > 0 ? diff: 0, //counterRed,
            counterRed: diff < 0 ? diff*(-1): 0,
        })
    })
)(SwapHeader);





