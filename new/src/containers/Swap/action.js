export const ADD_SWAP = '@@SWAP/ADD';
export const REMOVE_SWAP = '@@SWAP/REMOVE';

export const actionCreators = {
    addSwap: (swap) => {
        return ({
            type: ADD_SWAP,
            payload: { ...swap }
        })
    }, 
    removeSwap: () => {
        return ({
            type: REMOVE_SWAP,
            payload: { walkId, ownerId, substituteId }
        })
    }
};
