export const PUT_MY_LOCATION = '@@MY/PUT_MY_LOCATION';
export const SHARE_MY_LOCATION = '@@MY/SHARE_MY_LOCATION';

export const actionCreators = {
    putMyLocation: (coordinate, what) => {
        return ({
            type: PUT_MY_LOCATION,
            payload: coordinate, 
            meta: what
        })
    },  
    shareMyLocation: (coordinate) => {
        return ({
            type: SHARE_MY_LOCATION,
            payload: coordinate
        })
    },  
};