import { PUT_MY_LOCATION } from './action';

const initiaState = {}

export function myLocationReducer(state=initiaState, action){
    switch (action.type) {

        case PUT_MY_LOCATION:
          return action.payload;
    
        default:
          return state;
    };
   }
   
   export default myLocationReducer;