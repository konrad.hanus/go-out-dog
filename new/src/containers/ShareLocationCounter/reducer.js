import { INCREASE_LOCATION_COUNTER, DECRERSE_LOCATION_COUNTER } from './action';

const initiaState = 5

export function shareLocationCounterReducer(state=initiaState, action){
    switch (action.type) {

        case DECRERSE_LOCATION_COUNTER:       
          return state - 1;

        case INCREASE_LOCATION_COUNTER:
            return state + 1;
    
        default:
          return state;
    };
   }
   
   export default shareLocationCounterReducer;