import React from 'react';
import {
	Body,
	Container,
	Header,
	Left,
	Title,
	Form,
	Text,
	Item,
	Input,
	Label,
	Icon,
	Button,
	CheckBox,
	ListItem, 
} from 'native-base';
import { connect } from 'react-redux';
import { actionCreators } from './action';
import PAGE from "./../../pages/pages";


export class RegistrationForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			login: '',
			email: '',
			emailRepeted: '',
			password: '',
			passwordRepeted: '',
			checkbox: false
		};
	}

	componentDidUpdate(){
		
	}

	render() {
		return (
			<Container>
				<Container>
					<Form>
						<Item floatingLabel>
							<Icon active name="account" type="MaterialCommunityIcons" />
							<Label>Login</Label>
							<Input onChangeText={(value)=>this.setState({login: value})}/>
						</Item>
						<Item floatingLabel>
							<Icon active name="email" type="MaterialCommunityIcons" />
							<Label>E-mail</Label>
							<Input keyboardType="email-address" autoCorrect={false} autoCapitalize="none" onChangeText={(value)=>this.setState({email: value})}/>
						</Item>
						<Item floatingLabel>
							<Icon active name="email-check" type="MaterialCommunityIcons" />
							<Label>Powtórz e-mail</Label>
							<Input keyboardType="email-address" autoCorrect={false} autoCapitalize="none"  onChangeText={(value)=>this.setState({emailRepeted: value})}/>
						</Item>
						<Item floatingLabel>
							<Icon active name="lock" type="MaterialCommunityIcons" />
							<Label>Hasło</Label>
							<Input secureTextEntry={true} onChangeText={(value)=>this.setState({password: value})}/>
						</Item>
						<Item floatingLabel>
							<Icon active name="security" type="MaterialCommunityIcons" />
							<Label>Powtórz Hasło</Label>
							<Input secureTextEntry={true} onChangeText={(value)=>this.setState({passwordRepeted: value})}/>
						</Item>
						<ListItem>
							<CheckBox checked={true} />
							<Body>
								<Text>Akceptuje regulamin</Text>
							</Body>
						</ListItem>
						<Button full success onPress={() => this.props.signUp({...this.state, redirect: this.props.navigation.navigate})}>
							<Text>Zarejestruj się</Text>
						</Button>
					</Form>
					<Text> </Text>
						<Button full onPress={()=>(this.props.navigation.navigate(PAGE.LOGIN) )}><Text>Zaloguj się</Text></Button>
				</Container>
			</Container>
		);
	}
}

export default connect((state) => {
	return {
		auctions: state.auctions
	};
}, actionCreators)(RegistrationForm);
