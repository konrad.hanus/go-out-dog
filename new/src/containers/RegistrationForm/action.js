export const SIGNUP = '@@SECURITY_FUND/SIGNUP';
export const LOGIN = '@@SECURITY_FUND/LOGIN';
export const LOGOUT = '@@SECURITY_FUND/LOGOUT';
export const LOGIN_SUCCESS = '@@SECURITY_FUND/LOGIN_SUCCESS';

export const actionCreators = {
    signUp: (payload) => ({
        type: SIGNUP, 
        payload
    }), 
    login: (payload) => ({
        type: LOGIN, 
        payload
    }),
    logOut: (payload) => ({
        type: LOGOUT, 
        payload
    }),
    logout: (payload) => ({
        type: LOGOUT, 
        payload
    }), 
    loginSuccess: (payload) => ({
        type: LOGIN_SUCCESS,
        payload
    })
};