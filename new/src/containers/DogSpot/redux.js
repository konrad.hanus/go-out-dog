
import { PUT_DOG_SPOTS_LOCATION } from './action';

const initiaState = []

export function dogSpotsReducer(state=initiaState, action){
    switch (action.type) {

        case PUT_DOG_SPOTS_LOCATION:
          return action.payload;
    
        default:
          return state;
    };
   }
   
   export default dogSpotsReducer;