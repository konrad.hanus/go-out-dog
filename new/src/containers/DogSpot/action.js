export const FETCH_DOG_SPOTS_LOCATION = '@@FETCH/DOG_SPOTS_LOCATION';
export const PUT_DOG_SPOTS_LOCATION = '@@PUT/DOG_SPOTS_LOCATION';
export const REMOVE_DOG_SPOT_LOCATION = '@@PUT/REMOVE_DOG_SPOT_LOCATION';

export const actionCreators = {
    fetchDogSpotsLocation: () => {

        // console.log('akcja w');
        return ({
            type: FETCH_DOG_SPOTS_LOCATION,
        })
    },  
    putDogSpotsLocation: (dogspot) => {
        return ({
            type: PUT_DOG_SPOTS_LOCATION,
            payload: dogspot
        })
    },
    removeDogSpotLocation: (id) => {
        return ({
            type: REMOVE_DOG_SPOT_LOCATION, 
            meta: id,
        })
    }  
};