import { CREATE_WALK, UPDATE_WALK, PUT_WALKS } from './action';
import { AsyncStorage } from 'react-native';

const _storeData = async (key, object) => {
    try {
        await AsyncStorage.setItem(
            key,
            JSON.stringify(object)
        );
    } catch (error) {
        // Error saving data
    }
};


function walksReducer(state = [], action) {
    switch (action.type) {

        case PUT_WALKS:{
            return action.payload;
        }

        case UPDATE_WALK:  
            state[action.meta] = {
                ...state[action.meta],
                ...action.payload
            }
            //_storeData('WALKS', state)
            return state;


        case CREATE_WALK:
            function isEmptyArray(array){
                return typeof array !== 'undefined' && array.length > 0
            }
            function isWalkExsistFilter(walk) {
                return walk.id === action.payload.id;
            }
            const isWalkExsists = isEmptyArray(state.filter(isWalkExsistFilter));
            

           
                        
            if(!isWalkExsists)
            {
                return [...state, ...[action.payload]];
            }
    }
    return state;
}

export default walksReducer;