import React, { useState, useRef, useEffect } from "react";
import { StyleSheet } from "react-native";
import ProfileAvatar from "../../component/ProfileAvatar";
import DogAvatar from "../../component/Organisms/DogAvatar";
import Menu from "../../component/Menu";
import Bag from "../../component/Bag";
import MapView, { Circle, Polygon } from "react-native-maps";
import Pin from "../../component/Pin";
import Doge from "../../component/Pin/Doge.jpeg";
import LargeMap from "../../component/LargeMap";
import { connect } from "react-redux";
import { actionCreators } from "../MyLocation/action";
import { actionCreators as actionCreatorsFriends } from "../MyFriends/action";
import { actionCreators as actionCreatorsTreasures } from "../Treasure/action";
import { actionCreators as actionCreatorsDogSpots } from "../DogSpot/action";
import { actionCreators as actionCreatorsDogGyms } from "../DogGym/action";

const ViewMap = (props) => {
  const mapRefLarge = useRef(null);
  const [userLocation, setUserLocation] = useState();
  const [sharedCamera, setSharedCamera] = useState();

  return (
    <>
      <>
        <ProfileAvatar
          onPress={() => {
            alert("Profil Pana");
          }}
          distance={0}
          speed={0}
        />
        <DogAvatar
          onPress={() => {
            alert("profil psa");
          }}
        />
        <Menu
          label={"Gra"}
          onPress={() => {
            props.fetchDogsGymLocation();
            props.fetchDogsSpotLocation();
            props.fetchTreasuresLocation();
            props.setPage("main");
          }}
        />
        <Bag
          onPress={() => {
            alert("Tutaj będzie to co znajdziesz, jak cos znajdziesz");
          }}
        />
      </>
      <LargeMap
        mapRef={mapRefLarge}
        setUserLocation={setUserLocation}
        userLocation={userLocation}
        handleZoom={props.handleZoomLarge}
        sharedCamera={sharedCamera}
        setSharedCamera={setSharedCamera}
      >
        {props.children}

        {props.friends &&
          Object.keys(props.friends).map((key, index) => {
            const location =
              props.friends[key][Object.keys(props.friends[key])[0]].location;
            return (
              location && <MapView.Marker
                onPress={() => {
                  alert(key);
                }}
                coordinate={location}
              >
                {
                  <Pin
                    image={Doge}
                    onPress={() => {
                      alert("profil Psa");
                    }}
                  />
                }
              </MapView.Marker>
            );
          })}
      </LargeMap>
    </>
  );
};

export default connect(
  (state) => {
    return {
      friends: state.friends,
      locationCounter: state.locationCounter,
      treasures: state.treasures,
      dogSpots: state.dogSpots,
      dogGyms: state.dogGyms,
    };
  },
  {
    setMyLocation: actionCreators.putMyLocation,
    fetchFriendsLocation: actionCreatorsFriends.fetchFriendsLocation,
    fetchTreasuresLocation: actionCreatorsTreasures.fetchTreasuresLocation,
    removeTreasureLocation: actionCreatorsTreasures.removeTreasureLocation,
    removeDogSpotLocation: actionCreatorsDogSpots.removeDogSpotLocation,
    fetchDogsSpotLocation: actionCreatorsDogSpots.fetchDogSpotsLocation,
    removeDogGymLocation: actionCreatorsDogGyms.removeDogGymLocation,
    fetchDogsGymLocation: actionCreatorsDogGyms.fetchDogGymsLocation,
  }
)(ViewMap);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
