import {
  CHANGE_NICK,
  CHANGE_EMAIL,
  CHANGE_PASSWORD,
  CHANGE_DOG_NAME,
  CHANGE_DOG_GENDER_MALE,
  CHANGE_DOG_GENDER_FEMALE,
  CHANGE_GENDER_MALE,
  CHANGE_GENDER_FEMALE,
  CHANGE_DOG_AGE,
  CHANGE_DOG_WEIGHT,
  CHANGE_DOG_LASTINJECTION,
  CHANGE_DOG_TYPE
} from "./action";

import DATA from "./registrationData";

export function registrationReducer(state = DATA, action) {
  switch (action.type) {
    case CHANGE_NICK:
      return { ...state, nick: action.payload };
    case CHANGE_EMAIL:
      return { ...state, email: action.payload };
    case CHANGE_PASSWORD:
      return { ...state, password: action.payload };

    case CHANGE_GENDER_MALE:
      return { ...state, gender: "Mężczyzna" };
    case CHANGE_GENDER_FEMALE:
      return { ...state, gender: "Kobieta" };

    case CHANGE_DOG_NAME:
      return { ...state, dogName: action.payload };
    case CHANGE_DOG_GENDER_MALE:
      return { ...state, dogGender: "Pies" };
    case CHANGE_DOG_GENDER_FEMALE:
      return { ...state, dogGender: "Suka" };

    case CHANGE_DOG_AGE:
      return { ...state, dogAge: action.payload };
    case CHANGE_DOG_WEIGHT:
      return { ...state, dogWeight: action.payload };
    case CHANGE_DOG_LASTINJECTION:
      return { ...state, dogLastInjection: action.payload };
    case CHANGE_DOG_TYPE:
      return {...state, dogType: action.payload};

    default:
      return state;
  }
}

export default registrationReducer;
