export const CHANGE_NICK = "@REGISTRATION/CHANGE_NICK";
export const CHANGE_EMAIL = "@REGISTRATION/CHANGE_EMAIL";
export const CHANGE_PASSWORD = "@REGISTRATION/CHANGE_PASSWORD";

export const CHANGE_GENDER_MALE = "@WIZARD/CHANGE_GENDER_MALE";
export const CHANGE_GENDER_FEMALE = "@WIZARD/CHANGE_GENDER_FEMALE";

export const CHANGE_DOG_NAME = "@WIZARD_DOGS/CHANGE_DOG_NAME";
export const CHANGE_DOG_GENDER_MALE = "@WIZARD_DOGS/CHANGE_DOG_GENDER_MALE";
export const CHANGE_DOG_GENDER_FEMALE = "@WIZARD_DOGS/CHANGE_DOG_GENDER_FEMALE";

export const CHANGE_DOG_AGE = "@WIZARD_YOUR_DOGS/CHANGE_DOG_AGE";
export const CHANGE_DOG_WEIGHT = "@WIZARD_YOUR_DOGS/CHANGE_DOG_WEIGHT";
export const CHANGE_DOG_LASTINJECTION = "@WIZARD_YOUR_DOGS/CHANGE_DOG_LASTINJECTION";

export const CHANGE_DOG_TYPE = "@WIZARD_DOGS/CHANGE_DOG_TYPE";

export const actionCreators = {
  changeNick: (payload) => ({
    type: CHANGE_NICK,
    payload,
  }),
  changeEmail: (payload) => ({
    type: CHANGE_EMAIL,
    payload,
  }),
  changePassword: (payload) => ({
    type: CHANGE_PASSWORD,
    payload,
  }),
  changeGenderMale: (payload) => ({
    type: CHANGE_GENDER_MALE,
    payload,
  }),
  changeGenderFemale: (payload) => ({
    type: CHANGE_GENDER_FEMALE,
    payload,
  }),
  changeDogName: (payload) => ({
    type: CHANGE_DOG_NAME,
    payload,
  }),
  changeDogGenderMale: (payload) => ({
    type: CHANGE_DOG_GENDER_MALE,
    payload,
  }),
  changeDogGenderFemale: (payload) => ({
    type: CHANGE_DOG_GENDER_FEMALE,
    payload,
  }),

  changeDogAge: (payload) => ({
    type: CHANGE_DOG_AGE,
    payload,
  }),
  changeDogWeight: (payload) => ({
    type: CHANGE_DOG_WEIGHT,
    payload,
  }),
  changeDogLastInjection: (payload) => ({
    type: CHANGE_DOG_LASTINJECTION,
    payload,
  }),
  changeDogType: (payload) => ({
    type: CHANGE_DOG_TYPE,
    payload,
  })

};
