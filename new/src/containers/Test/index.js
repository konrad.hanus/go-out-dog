import React from 'react';
import {
    Text, Button
} from 'native-base';
import { connect } from 'react-redux';
import { actionCreators } from '../Walk/action';

export class Test extends React.Component {

    onSave = () => {
        this.props.saveWalks(this.props.walks)
    }

    onFetch = () => {
        this.props.fetchWalksDB()
    }

    render() {

        return (<React.Fragment>
            <Button onPress={() => this.onSave()}>
                <Text>Save</Text>
            </Button>
            <Button onPress={() => this.onFetch()}>
                <Text>Fetch</Text>
            </Button>
            <Text>Test {this.props.test}</Text></React.Fragment>)
    }
}

export default connect(
    ((state) => {
        return ({
            walks: state.walks
        })
    }), (actionCreators)
)(Test);

