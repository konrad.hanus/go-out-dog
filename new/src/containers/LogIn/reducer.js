import { SPINNER_ON, SPINNER_OFF } from './action';

const initiaState = {
    spinner: true
}

export function loginReducer(state=initiaState, action){
    switch (action.type) {

        case SPINNER_ON:
          return {spinner : action.payload };

        case SPINNER_OFF:
          return {spinner : action.payload };
    
        default:
          return state;
    };
   }
   
   export default loginReducer;