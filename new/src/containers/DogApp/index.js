import React, { useState, useRef, useEffect } from "react";
import { StyleSheet} from "react-native";
import { NativeBaseProvider } from "native-base";
import { connect } from "react-redux";

import Three from "../Three";
import ViewPlayfield from "../ViewPlayfield";
import DogGymsPin from "../../component/Organisms/DogGymsPin";
import DogSpotPin from "../../component/Organisms/DogSpotPin";
import TreasuresPin from "../../component/Organisms/TreasuresPin";

import { actionCreators } from "../MyLocation/action";
import { actionCreators as actionCreatorsFriends } from "../MyFriends/action";
import { actionCreators as actionCreatorsTreasures } from "../Treasure/action";
import { actionCreators as actionCreatorsDogSpots } from "../DogSpot/action";
import { actionCreators as actionCreatorsDogGyms } from "../DogGym/action";

console.disableYellowBox = true;

const DogApp = (props) => {

  const mapRef = useRef(null);
  const mapRefLarge = useRef(null);

  const [page, setPage] = useState("main");
  const [userLocation, setUserLocation] = useState();
  const [sharedCamera, setSharedCamera] = useState();
  const [coutnerLoaded,setCoutnerLoaded] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    props.fetchDogGymLocation();
    props.fetchDogsSpotLocation();
  }, [])

  const handleZoom = async (heading, angle, scale, duration) => {
    const camera = await mapRef.current.getCamera();

    mapRef.current.animateCamera(
      {
        center: {
          latitude: userLocation.latitude ? userLocation.latitude : 0,
          longitude: userLocation.longitude ? userLocation.longitude : 0,
        },
        heading: heading ? heading : camera.heading,
        pitch: 10,
        zoom: scale, //? scale : camera.scale,
        // pitch: angle ? (-angle > 20 ? -angle : 15) : camera.angle, //angle
        pitch: 65,
      },
      { duration: duration ? duration : 20 }
    );

    if (mapRefLarge.current) {
      const cameraToSave = await mapRefLarge.current.getCamera();
      setSharedCamera(cameraToSave);
    }
  };

  const dogSpotArray =  Object.keys(props.dogSpots);
  const treasuresArray =  Object.keys(props.treasures);
  const dogGymsArray =  Object.keys(props.dogGyms);

  const countDogspot = dogSpotArray ? dogSpotArray.length : 0;
  const countTreasures = treasuresArray ? treasuresArray.length : 0;
  const countDogGyms = dogGymsArray ? dogGymsArray.length : 0;
  const sumItems = countDogspot+countTreasures+countDogGyms+2;

  let a = 0;
  const incrementCounterLoaded = () => {
    
    a = a+1;
    log('test', a,coutnerLoaded)
    setCoutnerLoaded(a);
  }

  return (
    <ViewPlayfield 
    setPage={setPage} 
    handleZoom={handleZoom} 
    three={Three} 
    sumItems={sumItems} 
    incrementCounterLoaded={incrementCounterLoaded} 
    nextI={coutnerLoaded+2} 
    isLoading={isLoading}>
        <DogSpotPin dogSpots={props.dogSpots} removeDogSpotLocation={props.removeDogSpotLocation} incrementCounterLoaded={incrementCounterLoaded} 
      setIsLoading={setIsLoading} />
      
      {/* <TreasuresPin treasures={props.treasures} removeTreasureLocation={props.removeTreasureLocation} incrementCounterLoaded={incrementCounterLoaded} />
      <DogGymsPin dogGyms={props.dogGyms} removeDogGymLocation={props.removeDogGymLocation} incrementCounterLoaded={incrementCounterLoaded}/>   */}
    </ViewPlayfield>
  );
};

export default connect(
  (state) => {
    return {
      friends: state.friends,
      locationCounter: state.locationCounter,
      treasures: state.treasures,
      dogSpots: state.dogSpots,
      dogGyms: state.dogGyms,
    };
  },
  {
    setMyLocation: actionCreators.putMyLocation,
    fetchFriendsLocation: actionCreatorsFriends.fetchFriendsLocation,
    fetchTreasuresLocation: actionCreatorsTreasures.fetchTreasuresLocation,
    removeTreasureLocation: actionCreatorsTreasures.removeTreasureLocation,
    removeDogSpotLocation: actionCreatorsDogSpots.removeDogSpotLocation,
    fetchDogsSpotLocation: actionCreatorsDogSpots.fetchDogSpotsLocation,
    removeDogGymLocation: actionCreatorsDogGyms.removeDogGymLocation,
    fetchDogGymLocation: actionCreatorsDogGyms.fetchDogGymsLocation,
  }
)(DogApp);